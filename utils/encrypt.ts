export const pub = `
-----BEGIN PUBLIC KEY-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDA9FBVPqESqji48ipLB8ybtGjm
kBe4GnYgYS4WKGAnZcKD2H3keF/8eLHjqcD9VBreyFHq1XaTFK/NSkcPobURyiZf
veplJ1cICnXbm+QdTlDPey3VlRjlrCeTuLKsFqbCMzcVAdcReDil/NujQmsDu+yv
Rghq/AhufUIxSGjSSwIDAQAB
-----END PUBLIC KEY-----`;

const RASEncrypt = () => {
  //@ts-ignore
  if (window.jsencrypt) {
    //@ts-ignore
    return window.jsencrypt;
  } else {
    const { JSEncrypt } = require("jsencrypt");
    const instance = new JSEncrypt();
    instance.setPublicKey(pub);
    //@ts-ignore
    window.jsencrypt = instance;
    return instance;
  }
};

export default RASEncrypt;
// export const encrypt = (data: string) => instance.encrypt(data);
