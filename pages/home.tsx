import SEO from "../layout/SEO";
import { ReactNode } from "react";
import Main from "../components/HomePage/Main";

function Home(): ReactNode {
  return (
    <div>
      <SEO title="宏斌的博客" />
      <Main />
    </div>
  );
}

export default Home;
