import { FC, ReactElement } from "react";
import styled from "styled-components";
import SEO from "../layout/SEO";
import { WaveText } from "../components/BUI";
import SwipeBlog from "../components/Home/SwipeBlog";
import Quality from "../components/Home/Quality";
import { ContainerCss } from "../components/commentStyled";
import TagWall from "../components/Home/TagWall";
import Introduce from "../components/Home/Introduce";

interface IProps {}

const Home: FC<IProps> = (): ReactElement => {
  return (
    <Container>
      <SEO title="宏斌的博客-欢迎👏" logo="hello.svg" />
      <WaveText />
      <SwipeBlog />
      <Quality />
      <TagWall />
      <Introduce />
    </Container>
  );
};

export default Home;

const Container = styled.div`
  min-height: 100vh;
  ${ContainerCss};
  display: flex;
  align-items: center;
  flex-direction: column;
  [data-wave-text] {
    margin: 10rem 0;
  }
`;
