import { FC, ReactElement, useRef, useState } from "react";
import styled from "styled-components";
import { Button, FlexDiv, TextInput } from "../components/BUI";
import { randomColor } from "../components/BUI/DateTimePicker/utils";
import { flexCenter, scrollbar } from "../components/BUI/styled";
import { ThemeBgcAndText } from "../components/BUI/View/ThemeView";
import { ContainerCss } from "../components/commentStyled";
import Disc from "../components/Turntable/Disc";
import SEO from "../layout/SEO";

type IList = Array<{ color: string; content: string }>;

const conf1 = [
  { color: "#ffaaaa", content: "5食堂" },
  { color: "#0000ff", content: "6食堂" },
  { color: "#f7d303", content: "外卖" },
];

const Turntable: FC = (): ReactElement => {
  const [list, setList] = useState<IList>([]);
  const inputRef = useRef<HTMLInputElement>(null);
  const [select, setSelect] = useState(-1); //-1 未抽
  const [showResult, setShowResult] = useState(false);
  const appoint = useRef(-1); //指定下一次的值 页面不需要重新渲染 因此使用useRef

  const addOption = () => {
    const input = inputRef.current;
    if (input) {
      const content = input.value;
      if (content) {
        // 确保颜色唯一
        let color = randomColor();
        while (
          color === "#000000" ||
          list.some(option => option.color == color)
        ) {
          console.log("颜色重复", color);
          color = randomColor();
        }
        setList(list => [...list, { color, content }]);
        input.value = "";
      }
    }
  };

  const delOption = (option: string) => {
    setList(list => list.filter(item => item.content != option));
  };

  const handleLuckDraw = () => {
    if (!list.length) return;
    let index;
    //如果没有指定结果随机确定一个选项
    if (appoint.current == -1) {
      index = Math.floor(Math.random() * list.length);
    } else {
      index = appoint.current;
      //   @ts-ignore
      appoint.current = -1;
    }
    setSelect(index);
    setTimeout(() => {
      setShowResult(true);
    }, 3000);
  };

  return (
    <Container>
      <SEO title="hello转盘" logo="/turntable_logo.svg" />
      <FlexDiv column>
        <FlexDiv
          width="100%"
          height="4rem"
          items="center"
          justify="center"
          space="10px"
        >
          <TextInput
            style={{ flex: 1 }}
            placeholder="添加选项"
            width="20rem"
            ref={inputRef}
          />
          <Button onClick={addOption} width="6rem">
            添加
          </Button>
        </FlexDiv>
        <Button margin width="6rem" onClick={() => setList(conf1)}>
          使用配置1
        </Button>
        <FlexDiv column>
          {list.map(({ color, content }, index) => (
            <FlexDiv
              style={{ marginTop: "1rem" }}
              key={index + content}
              items="center"
              onDoubleClick={() => (appoint.current = index)}
            >
              <Button
                onClick={() => delOption(content)}
                margin
                width="7rem"
                lightBgc={color}
                darkBgc={color}
              >
                撤销 - {color}
              </Button>
              {content}
            </FlexDiv>
          ))}
        </FlexDiv>
      </FlexDiv>
      <FlexDiv column items="center" justify="center">
        <Disc list={list} select={select} />
        <Button
          onClick={handleLuckDraw}
          height="6rem"
          width="8rem"
          column
          padding="0.3rem"
        >
          <svg
            viewBox="0 0 1024 1024"
            version="1.1"
            xmlns="http://www.w3.org/2000/svg"
            p-id="3621"
            width="64"
            height="64"
          >
            <path
              d="M19.059101 19.059101h985.881798v985.881798H19.059101z"
              fill="#E7335B"
              p-id="3622"
            ></path>
            <path
              d="M512 95.517767c-230.018977 0-416.490171 186.463256-416.490171 416.482233S281.981023 928.490171 512 928.490171c230.018977 0 416.490171-186.471194 416.490171-416.490171 0-230.018977-186.471194-416.490171-416.490171-416.490171z m0 454.370233a37.895938 37.895938 0 1 1 0-75.783938 37.895938 37.895938 0 0 1 0 75.791876z"
              fill="#3FA3A3"
              p-id="3623"
            ></path>
            <path
              d="M840.616682 19.059101h164.324217v164.324217h-164.324217zM512 358.193612c-84.936434 0-153.806388 68.862016-153.806388 153.806388S427.055628 665.806388 512 665.806388 665.806388 596.944372 665.806388 512 596.944372 358.193612 512 358.193612z m0 191.702326a37.895938 37.895938 0 1 1 0-75.791876 37.895938 37.895938 0 0 1 0 75.791876z"
              fill="#F5AF47"
              p-id="3624"
            ></path>
            <path
              d="M825.54245 3.968992H3.968992v1016.062016h1016.062016V3.968992h-194.488558z m164.308341 30.180217V168.285271h-108.758326l59.828589-63.876961-22.027907-20.63876-63.178418 67.449055V34.157147h134.136062z m0 955.701582H34.149209V34.149209h791.393241v149.289675l-15.447318 16.487194C729.445209 122.80062 623.973209 80.419721 512 80.419721c-115.275411 0-223.652713 44.89724-305.167876 126.412403C125.316961 288.347287 80.411783 396.724589 80.411783 512c0 115.275411 44.89724 223.652713 126.412403 305.167876C288.347287 898.683039 396.724589 943.572341 512 943.572341c115.275411 0 223.652713-44.889302 305.167876-126.404465 81.515163-81.515163 126.404465-189.892465 126.404465-305.167876 0-108.480496-39.753426-210.832868-112.370108-290.458791l21.623069-23.083659h137.025489v791.393241z m-269.653334-693.954481C666.179473 243.83107 592.768992 211.761612 512 211.761612c-165.554605 0-300.238388 134.691721-300.238388 300.238388h30.180217c0-148.908651 121.149519-270.050233 270.058171-270.050233 72.807194 0 138.962357 28.965705 187.574574 75.966512l-41.15845 43.936744 40.078884 62.392558 25.393612-16.30462-27.37017-42.611101 23.615503-25.218976c38.657984 46.730915 61.924217 106.646822 61.924218 171.889116 0 148.916589-121.149519 270.050233-270.058171 270.050233v30.188155c165.554605 0 300.238388-134.691721 300.238388-300.238388 0-73.92645-26.870078-141.677147-71.338667-194.051969l69.465302-74.156651c64.011907 71.140217 103.035039 165.197395 103.035039 268.20862 0 221.326884-180.06524 401.392124-401.392124 401.392124-221.334822 0-401.408-180.06524-401.408-401.392124 0-221.334822 180.06524-401.400062 401.400062-401.400062 107.472372 0 205.212775 42.468217 277.337302 111.488992l-69.139845 73.80738z"
              fill="#871F4D"
              p-id="3625"
            ></path>
            <path
              d="M343.103504 512c0 93.128434 75.768062 168.896496 168.896496 168.896496 93.128434 0 168.896496-75.768062 168.896496-168.896496 0-93.128434-75.768062-168.896496-168.896496-168.896496-93.128434 0-168.896496 75.768062-168.896496 168.896496z m307.612775 0c0 76.490419-62.233798 138.716279-138.716279 138.716279-76.490419 0-138.716279-62.233798-138.716279-138.716279 0-76.490419 62.233798-138.716279 138.716279-138.716279 76.490419 0 138.716279 62.233798 138.716279 138.716279z"
              fill="#871F4D"
              p-id="3626"
            ></path>
            <path
              d="M459.013953 512A53.041612 53.041612 0 0 0 512 564.986047a53.04955 53.04955 0 0 0 52.986047-52.986047A53.04955 53.04955 0 0 0 512 459.013953a53.041612 53.041612 0 0 0-52.986047 52.986047z m75.791876 0A22.829643 22.829643 0 0 1 512 534.805829a22.829643 22.829643 0 0 1-22.805829-22.805829A22.829643 22.829643 0 0 1 512 489.194171a22.829643 22.829643 0 0 1 22.805829 22.805829z"
              fill="#871F4D"
              p-id="3627"
            ></path>
            <path
              d="M146.090667 512c0 201.759752 164.149581 365.909333 365.909333 365.909333 201.759752 0 365.909333-164.149581 365.909333-365.909333h-30.180217c0 185.113798-150.60738 335.729116-335.729116 335.729116-185.113798 0-335.729116-150.60738-335.729116-335.729116 0-185.113798 150.60738-335.729116 335.729116-335.729116v-30.180217c-201.759752 0-365.909333 164.149581-365.909333 365.909333z"
              fill="#871F4D"
              p-id="3628"
            ></path>
            <path
              d="M512 746.567442c129.341519 0 234.567442-105.225922 234.567442-234.567442h-30.180217c0 112.695566-91.683721 204.387225-204.387225 204.387225-112.695566 0-204.387225-91.683721-204.387225-204.387225 0-112.695566 91.683721-204.387225 204.387225-204.387225V277.432558c-129.341519 0-234.567442 105.225922-234.567442 234.567442S382.658481 746.567442 512 746.567442z"
              fill="#871F4D"
              p-id="3629"
            ></path>
          </svg>
          BEGIN
        </Button>
        {list.length && select !== -1 ? (
          <Mask show={showResult}>
            <div style={{ background: list[select].color }}>
              {list[select].content}
            </div>
            <Button
              width="10rem"
              onClick={() => {
                setShowResult(false);
                setSelect(-1);
              }}
              lightBgc={list[select].color}
              darkBgc={list[select].color}
            >
              关闭
            </Button>
          </Mask>
        ) : null}
      </FlexDiv>
    </Container>
  );
};

export default Turntable;

const Mask = styled.div<{ show: boolean }>`
  ${flexCenter};
  position: absolute;
  background-color: #00000055;
  width: 100%;
  height: 100%;
  left: 0;
  top: ${props => (props.show ? 0 : "100%")};
  flex-direction: column;
  z-index: 2;
  border-radius: 0.8rem;
  letter-spacing: 2px;
  & > div {
    padding: 2rem;
    margin-bottom: 1rem;
    font-size: 2rem;
    border-radius: inherit;
  }
`;

const Container = styled.div`
  ${ContainerCss};
  ${ThemeBgcAndText};
  display: flex;
  padding: 1rem;

  & > div {
    flex: 1;
    position: relative;
    :last-child {
      overflow: hidden;
    }
    :first-child {
      overflow: hidden scroll;
      ${scrollbar};
    }
    :first-child::before {
      content: "";
      position: absolute;
      height: 100%;
      width: 1px;
      right: 0;
      top: 0;
      background-color: #ccc;
    }
  }

  @media (max-width: ${props => props.theme.media.phone}) {
    flex-direction: column;
    & > div {
      padding-bottom: 1rem;
      :last-child {
        overflow: hidden;
        min-height: 600px;
        & > div:first-child {
          height: 320px;
          width: 320px;
        }
      }

      :first-child::before {
        width: 100%;
        height: 1px;
        top: auto;
        bottom: 0;
        left: 0;
      }
    }
  }
`;
