import { FC, ReactElement } from "react";
import styled from "styled-components";
import { getUserById, IUserDetail } from "../../api/userApi";
import { FlexDiv } from "../../components/BUI";
import { flexCenter } from "../../components/BUI/styled";
import { ContentWidth } from "../../components/commentStyled";
import RoleTag from "../../components/common/RoleTag";
import BlogCard from "../../components/BlogList/BlogCard";
import { Blog } from "../../api/types";
import SEO from "../../layout/SEO";

interface IProps {
  user: IUserDetail;
}

const User: FC<IProps> = ({ user }): ReactElement => {
  console.log("user:", user);
  return (
    <Container>
      <SEO title={`${user.username}-个人中心`} />
      <UserName>
        <span>{user.username}</span>
        <RoleTag role={user.role} />
        {user.mail && <span data-mail>mail:{user.mail}</span>}
      </UserName>
      <FlexDiv justify="space-between">
        <DataItem>
          <span>{user.blogCount}</span>
          <span>博客数</span>
        </DataItem>
        <DataItem>
          <span>{user.thumbsUp.length}</span>
          <span>点赞数</span>
        </DataItem>
        <DataItem>
          <span>{user.thumbsDown.length}</span>
          <span>点踩数</span>
        </DataItem>
        <DataItem>
          <span>{user.commentCount}</span>
          <span>评论数</span>
        </DataItem>
      </FlexDiv>
      {user.blogs.map((blog: Blog) => (
        <BlogCard blog={blog} key={blog._id} />
      ))}
    </Container>
  );
};

export default User;

export const getServerSideProps = async ({ query: { id } }: any) => {
  // 需要能转成json的数据 例如undefined则不行
  const { data = {} } = await getUserById(id);

  return {
    props: {
      user: data.user || "",
    },
  };
};

const DataItem = styled.div`
  background: linear-gradient(60deg, rgb(0, 195, 255), rgb(204, 0, 255));
  border-radius: 1rem;
  border-top-left-radius: 50%;
  padding: 1rem;
  flex-direction: column;
  ${flexCenter};
  letter-spacing: 1px;
  font-weight: bold;
  & > span:first-child {
    font-size: 2rem;
  }
  margin: 1rem;
`;

const UserName = styled.div`
  height: 4rem;
  width: 100%;
  flex-wrap: wrap;
  background: linear-gradient(60deg, rgb(0, 195, 255), rgb(204, 0, 255));
  border-radius: 0.5rem;
  overflow-x: scroll;

  letter-spacing: 2px;
  ${flexCenter};

  & > span:first-child {
    font-size: 2rem;
  }
  & > span[data-mail] {
    background: #6d05e46d;
    border-radius: 1rem;
    margin-left: 0.5rem;
    padding: 0.1rem 0.9rem;
  }
`;

const Container = styled.div`
  min-height: calc(100vh - var(--nav-height));
  ${ContentWidth};
  padding: 1rem;
  margin: 1rem auto;
  color: ${props => props.theme.lightText};
  border-radius: 1rem;
  overflow-x: hidden;
`;
