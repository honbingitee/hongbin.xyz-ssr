import styled from "styled-components";
import { flexCenter } from "../components/BUI/styled";

function Custom404() {
  return (
    <Container>
      <p>404</p>
    </Container>
  );
}

export default Custom404;

const Container = styled.div`
  height: calc(100vh - var(--nav-height) - var(--footer-height));
  ${flexCenter};

  & > p {
    font-size: 10rem;
    color: #fff;
    text-decoration: underline;
  }
`;
