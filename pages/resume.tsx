/*
 * @Author: 宏斌
 * @Date: 2021-10-08 20:09:43
 * @LastEditTime: 2021-11-21 21:13:43
 * @Description: 我的简历页
 * @FilePath: /hongbin_xyz_web/pages/resume.tsx
 */
import { FC, ReactElement, useCallback, useState } from "react";
import styled from "styled-components";
import { flexCenter, hideScrollbar, scrollbar } from "../components/BUI/styled";
import { ThemeBgcAndText } from "../components/BUI/View/ThemeView";
import { ContentWidth } from "../components/commentStyled";
import OptionButton from "../components/Resume/OptionButton";
import SEO from "../layout/SEO";
import ContactMe from "../components/Resume/ContactMe";
import ProjectExperience from "../components/Resume/ProjectExperience";
import TechnologyStack from "../components/Resume/TechnologyStack";
import Introduce from "../components/Resume/Introduce";
import OpenSource from "../components/Resume/OpenSource";

interface IProps {}

const Data = [
  { index: 5, content: "个人介绍", component: <Introduce /> },
  { index: 4, content: "项目经验", component: <ProjectExperience /> },
  { index: 3, content: "技术栈", component: <TechnologyStack /> },
  { index: 2, content: "开源作品", component: <OpenSource /> },
  { index: 1, content: "联系方式", component: <ContactMe /> },
];

const addIndex = (prev: any[]) =>
  prev.map((item: any) => ({
    ...item,
    index: item.index === prev.length ? 1 : item.index + 1,
  }));

const decreaseIndex = (prev: any[]) =>
  prev.map((item: any) => ({
    ...item,
    index: item.index === 1 ? prev.length : item.index - 1,
  }));

const Resume: FC<IProps> = (): ReactElement => {
  const [cards, setCards] = useState(Data);
  const { length } = cards;

  const handleChangeCard = useCallback(
    (type?: string) => () => {
      setCards(type === "add" ? addIndex : decreaseIndex);
    },
    []
  );

  /**
   * 跳转到第几张卡片
   */
  const jumpCard = (index: number) => () => {
    setCards(prev => {
      const diff = length - index;
      return prev.map(item => {
        let newIndex = item.index + diff;
        newIndex > length && (newIndex -= length);
        return { ...item, index: newIndex };
      });
    });
  };

  return (
    <Container>
      <SEO title="我的简历" />
      <Swipe>
        {cards.map(card => (
          <SwipeItem key={card.content} zIndex={card.index} length={length}>
            <div>
              <Title>{card.content}</Title>
              <Content showScrollBar={card.index === length}>
                {card.component}
              </Content>
              {card.index === length ? null : (
                <RightRange onClick={jumpCard(card.index)} />
              )}
              <OptionButton
                onClickUp={handleChangeCard()}
                onClickDown={handleChangeCard("add")}
              />
            </div>
          </SwipeItem>
        ))}
      </Swipe>
    </Container>
  );
};

export default Resume;

export const ImageRadius = styled.div`
  width: 200px;
  height: 200px;
  border-radius: 150px 48px 80px;
  overflow: hidden;
  transition: border-radius 0.3s linear;
`;

const Content = styled.div<{ showScrollBar: boolean }>`
  flex: 1;
  display: flex;
  flex-wrap: wrap;
  overflow: hidden scroll;
  ${({ showScrollBar }) => (showScrollBar ? scrollbar : hideScrollbar)};
  a {
    color: var(--primary-color);
    text-direction: underline;
  }
  :hover ${ImageRadius} {
    border-radius: 0;
  }
`;

const RightRange = styled.div`
  cursor: pointer;
  position: absolute;
  right: -1rem;
  top: -0.5rem;
  width: 2rem;
  height: 20rem;
  transition: background 0.3s 0.2s linear;
  :hover {
    background: var(--primary-color);
  }
`;

export const Title = styled.p`
  font-weight: bold;
  text-shadow: 2px 2px 2px #333;
  font-size: 1.5rem;
  margin-top: 0;
  margin-bottom: 0.5rem;
  letter-spacing: 5px;
`;

const SwipeItem = styled.div<{ length: number; zIndex: number }>`
  width: 70%;
  height: 20rem;
  ${ThemeBgcAndText};
  border-radius: 5px;
  box-shadow: 7px 6px 20px 0px #444;
  position: absolute;
  padding: 0.5rem 1rem;
  ${({ zIndex, length }) =>
    `transform: translate(${(length - zIndex) * 30}px,${
      (length - zIndex) * -30
    }px);
    z-index:${zIndex}`};
  transition: all 0.3s linear;

  @media (max-width: ${props => props.theme.media.phone}) {
    height: 40rem;
    ${({ zIndex, length }) =>
      `transform: translate(${(length - zIndex) * 20}px,${
        (length - zIndex) * -20
      }px);
    z-index:${zIndex}`};
  }

  & > div {
    position: relative;
    width: 100%;
    height: 100%;
    display: flex;
    flex-direction: column;
  }
  overflow: hidden;
`;

const Swipe = styled.div`
  width: 100%;
  height: 100%;
  position: relative;
  ${flexCenter};
  transform: translateX(-10%);
`;

const Container = styled.div`
  ${ContentWidth};
  height: calc(100vh - var(--nav-height));
  margin: 0 auto;
  user-select: none;
  overflow-x: hidden;

  @media (max-width: ${props => props.theme.media.phone}) {
    margin-top: 2rem;
    overflow: none;
    overflow-x: visible;
  }
`;
