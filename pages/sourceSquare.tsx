/*
 * @Author: hongbin
 * @Date: 2021-10-31 21:26:24
 * @LastEditTime: 2021-12-25 14:34:50
 * @Description: 资源广场
 * @FilePath: /hongbin_xyz_web/pages/sourceSquare.tsx
 */
import { FC, ReactElement, useEffect, useState } from "react";
import styled from "styled-components";
import { querySource, Source } from "../api/source";
import { flexCenter } from "../components/BUI/styled";
import { ThemeBgcAndText } from "../components/BUI/View/ThemeView";
import { ContainerCss } from "../components/commentStyled";
import { downloadSourcePath } from "../constant/url";
import SEO from "../layout/SEO";
import { downloadBlob, getExt, imageExt } from "../utils";

interface IProps {}

const SourceSquare: FC<IProps> = (): ReactElement => {
  const [sources, setSources] = useState<Source[]>([]);

  useEffect(() => {
    querySource().then(({ data }) => {
      console.log(data);
      setSources(data.sources);
    });
  }, []);

  return (
    <Container>
      <SEO title="资源广场" />
      {sources.map(source => (
        <SourceItem key={source._id} source={source} />
      ))}
    </Container>
  );
};

interface ISourceItem {
  source: Source;
}

const SourceItem: FC<ISourceItem> = ({
  source: {
    name,
    description,
    file,
    sender: [sender],
  },
}) => {
  return (
    <Box>
      <strong>{name}</strong>
      <span>{description}</span>
      <FileName>{file}</FileName>
      {/* 远程图片点击会在新标签页打开 不会下载 需转成blog下载 */}
      {imageExt.includes(getExt(file)) ? (
        <span
          data-a
          onClick={() => {
            fetch(`${downloadSourcePath}${file}`)
              .then(res => res.blob())
              .then(res => downloadBlob(res, file));
          }}
        >
          下载
        </span>
      ) : (
        <a href={`${downloadSourcePath}${file}`} download={file}>
          下载
        </a>
      )}
      {sender ? (
        <footer>
          发布者:
          <a
            href={`https://hongbin.xyz/user/${sender._id}`}
            target="_blank"
            rel="noreferrer"
          >
            {sender.username}
          </a>
        </footer>
      ) : null}
    </Box>
  );
};

export default SourceSquare;

const FileName = styled.span`
  background: #ccc;
  border-radius: 10px;
  width: 100%;
  height: 5rem;
  padding: 0.5rem;
  margin: 0.5rem 0;
  ${flexCenter};
`;

const Box = styled.div`
  ${ThemeBgcAndText};
  width: 20rem;
  height: 15rem;
  border-radius: 1rem;
  margin: 1rem;
  display: flex;
  flex-direction: column;
  padding: 0.5rem;
  padding-bottom: 2rem;
  position: relative;

  & > span {
    color: #999;
    font-size: 0.8rem;
  }

  footer {
    position: absolute;
    bottom: 0;
    width: 100%;
    padding: 0.2rem;
    height: 2rem;
    ${flexCenter};
    font-size: 0.8rem;
    z-index: 1;
  }
  a,
  [data-a] {
    color: var(--primary-color);
    cursor: pointer;
    font-size: 1rem;
  }
`;

const Container = styled.div`
  ${ContainerCss};
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  align-items: center;

  @media (max-width: ${props => props.theme.media.phone}) {
    flex-direction: column;
    align-items: center;
    & > div {
      width: 90%;
    }
  }
`;
