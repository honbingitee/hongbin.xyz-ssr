/*
 * @Author: hongbin
 * @Date: 2021-10-07 16:35:22
 * @LastEditTime: 2021-10-08 07:41:16
 * @Description: ASCII 表
 */
import { FC, ReactElement } from "react";
import styled from "styled-components";
import {
  ThemeBgcAndText,
  ThemePrimaryBgc,
} from "../components/BUI/View/ThemeView";
import { ContentWidth } from "../components/commentStyled";
import ASCIIS, { ASCIIDescription } from "../constant/ASCIIS";
import SEO from "../layout/SEO";

interface IProps {}

const ASCIITable: FC<IProps> = (): ReactElement => {
  return (
    <Container>
      <SEO title="ASCII对照表" />
      <Introduce>
        <span>{ASCIIDescription.desc}</span>
        <a href={ASCIIDescription.link}>百度百科</a>
      </Introduce>
      <Table>
        <thead>
          <tr>
            <td>二进制</td>
            <td>十进制</td>
            <td>十六进制</td>
            <td>缩写/字符</td>
            <td>解释</td>
          </tr>
        </thead>
        <tbody>
          {ASCIIS.map(({ b, dec, hex, char, explain, abbr }) => (
            <tr data-char={!!char} key={b}>
              <td>{b}</td>
              <td>{dec}</td>
              <td>{hex}</td>
              <td>{char || abbr}</td>
              <td>{explain}</td>
            </tr>
          ))}
        </tbody>
      </Table>
    </Container>
  );
};

export default ASCIITable;

const Introduce = styled.div`
  width: 100%;
  padding: 0.5rem;
  a {
    color: var(--primary-color);
  }
`;

const Table = styled.table`
  overflow: hidden scroll;
  width: 100%;
  border-spacing: 0;

  td {
    padding: 0.5rem;
  }

  tbody {
    tr {
      transition: background 0.3s linear;
    }
    tr:hover {
      transition: all 0.3s 0.3s linear;
      background: #000;
      color: #ccc;
    }
  }

  thead {
    ${ThemePrimaryBgc};
    font-weight: bold;
    letter-spacing: 2px;
    color: #fff;
    position: -webkit-sticky;
    position: sticky;
    top: var(--nav-height);

    td {
      position: relative;
      text-shadow: 0 0 2px #000;
      padding: 0.7rem 0.5rem;

      ::after {
        content: "";
        position: absolute;
        top: 25%;
        right: 0;
        width: 1px;
        height: 50%;
        background: #ccc;
        border-radius: 2px;
        box-shadow: 0 0 2px 1px #777;
      }
    }
    td:last-child {
      ::after {
        display: none;
      }
    }
  }
`;

const Container = styled.div`
  ${ContentWidth};
  margin: 0 auto;
  ${ThemeBgcAndText};
  min-height: 100vh;
`;
