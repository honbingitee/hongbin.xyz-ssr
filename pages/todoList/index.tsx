import React, { FC, Fragment, ReactElement } from "react";
import { getTodoList } from "../../api/todoList";
import SEO from "../../layout/SEO";
import { Container } from "../blogList";
import styled, { css } from "styled-components";
import { flexCenter, scrollbar } from "../../components/BUI/styled";
import { ThemeBgcAndText } from "../../components/BUI/View/ThemeView";

interface IProps {
  todoList: Array<{ _id: string; name: string; result?: string }>;
}

const TodoList: FC<IProps> = ({ todoList }): ReactElement => {
  const todo = todoList.filter(item => !item.result);
  const undo = todoList.filter(item => item.result);

  return (
    <Wrap>
      <SEO title="代办事项" />
      <div>
        {todo.map(({ _id, name }) => (
          <TodoItem key={_id}>{name}</TodoItem>
        ))}
      </div>
      <div>
        {undo.map(({ _id, name, result }) => (
          <Fragment key={_id}>
            <TodoItem>{name}</TodoItem>
            <Result>解决：{result}</Result>
          </Fragment>
        ))}
      </div>
    </Wrap>
  );
};

const Result = styled.div`
  ${ThemeBgcAndText};
  padding: 0.5rem;
  font-size: 0.9rem;
  width: calc(100% - 1.5rem);
  margin: 0 auto;
  text-align: center;
  border-bottom-left-radius: 1rem;
  border-bottom-right-radius: 1rem;
  ${props => props.theme.mode === "dark" && "filter: brightness(0.5);"};
`;

const TodoItem = styled.div`
  width: 100%;
  min-height: 4rem;
  background: #fff;
  margin-top: 1rem;
  padding: 1rem;
  border-radius: 1rem;
  ${flexCenter};
  color: ${props => props.theme.lightText};
  background: ${() =>
    `rgba(${Math.ceil(Math.random() * 200)},${Math.ceil(
      Math.random() * 200
    )},${Math.ceil(Math.random() * 200)})`};
  transition: transform 0.2s linear, box-shadow 0.4s linear;

  &:hover {
    box-shadow: 0 0 0.5rem 0 #666;
    transform: translateY(-2px);
  }
  ${props => props.theme.mode === "dark" && "filter: brightness(0.5);"};
`;

const before = css`
  text-align: center;
  font-size: 1.2rem;
  font-weight: bold;
  display: inline-block;
  width: 100%;
  color: ${props => props.theme.lightText};
`;

const Wrap = styled(Container)`
  display: flex;
  & > div {
    flex: 1;
    padding: 1rem;
    overflow-y: scroll;
    ${scrollbar};
  }
  & > div:first-child {
    &::before {
      content: "待完成";
      ${before};
    }
  }
  & > div:last-child {
    &::before {
      content: "已完成";
      ${before};
    }
  }
`;

//@ts-ignore
TodoList.getInitialProps = async () => {
  const { data = {} } = await getTodoList();

  return { todoList: data.todoList || [] };
};

// export async function getServerSideProps() {
//   await new Promise((resolve, reject) => {
//     setTimeout(() => {
//       resolve(111);
//     }, 2000);
//   });
//   // const data = await res.json()
//   console.log("process.env:", process.env);
//   return {
//     props: { id: 1, url: process.env.NEXT_PUBLIC_URL },
//   };
// }

export default TodoList;
