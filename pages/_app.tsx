import type { AppProps } from "next/app";
import ThemeProvide from "../context/ThemeProvide";
import Header from "../layout/Header";
import UserProfile from "../layout/UserProfile";
import Banner from "../components/HomePage/Banner";
import ScrollToTop from "../components/HomePage/ScrollToTop";
import Footer from "../layout/Footer";
import AnnouncementBar from "../layout/AnnouncementBar";
import AuthProvide from "../context/AuthProvide";
import "../styles/globals.css";
import WorldNews from "../components/WorldNews";
import NextNProgress from "nextjs-progressbar";
import Bullet from "../components/Bullet";
import MobileBar from "../components/MobileBar";

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <>
      <NextNProgress color="#faa" />
      <AuthProvide>
        <ThemeProvide>
          <AnnouncementBar />
          <Header />
          <UserProfile />
          <Banner />
          <ScrollToTop />
          <WorldNews />
          <Component {...pageProps} />
          <Footer />
          <MobileBar />
          <Bullet />
        </ThemeProvide>
      </AuthProvide>
    </>
  );
}

export default MyApp;
