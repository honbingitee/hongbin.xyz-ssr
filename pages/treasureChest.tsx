import { FC, ReactElement, useState } from "react";
import styled from "styled-components";
import { ThemeBgcAndText } from "../components/BUI/View/ThemeView";
import { ContentWidth } from "../components/commentStyled";
import Image from "next/image";
import { getImagePath } from "../constant/url";
import SEO from "../layout/SEO";
import { useMount } from "../hooks";
import { ITreasure, queryTreasureChest } from "../api/treasureChest";

interface IProps {}

const TreasureChest: FC<IProps> = (): ReactElement => {
  const [treasures, setTreasures] = useState<ITreasure[]>([]);

  useMount(() => {
    queryTreasureChest().then(({ data }) => {
      if (data) {
        setTreasures(data.treasures);
      }
    });
  });

  return (
    <Container>
      <SEO title="百宝箱" />
      {treasures.map(({ _id, name, description, url, images: [image] }) => (
        <Box key={_id}>
          <ImageWrap>
            <Image src={`${getImagePath}${image}`} alt={image} layout="fill" />
          </ImageWrap>
          <Description>
            <h3
              onClick={() => {
                window.open(url, "_blank");
              }}
            >
              {name}
            </h3>
            <br />
            <span>{description}</span>
            <a href={url} target="_blank" rel="noreferrer" />
          </Description>
        </Box>
      ))}
    </Container>
  );
};

export default TreasureChest;

const Description = styled.div`
  margin-left: 0.5rem;
  h3 {
    letter-spacing: 2px;
    display: inline-block;
    transition: all 0.3s linear;
    position: relative;

    ::before {
      content: "";
      position: absolute;
      bottom: 0;
      left: 0%;
      width: 50%;
      height: 1px;
      background: ${props => props.theme.text};
      transition: left 0.3s linear, opacity 0.4s ease, width 0.3s linear;
      opacity: 0;
    }

    ::after {
      content: "";
      position: absolute;
      bottom: 0;
      left: 50%;
      width: 5px;
      height: 5px;
      border: 1px solid transparent;
      border-bottom-color: ${props => props.theme.text};
      transform: rotate(320deg) translate(-100%, -50%);
      opacity: 0;
      transition: inherit;
    }

    :hover {
      text-shadow: 2px 1px #999;
      transform: translateY(1px);
      ::before {
        opacity: 1;
        left: 10%;
        width: 100%;
      }
      ::after {
        opacity: 1;
        left: 110%;
      }
    }
  }
  span {
    font-size: 0.9rem;
  }

  &:hover {
    a {
      bottom: 0.5rem;
    }
  }
  a {
    display: inline-block;
    width: 1rem;
    height: 1rem;
    border: 2px solid ${props => props.theme.text};
    border-left: none;
    border-bottom: none;
    position: absolute;
    border-radius: 2px;
    bottom: 0.5rem;
    right: 0.5rem;
    transform: rotate(45deg);
    transition: all 0.3s linear;
    cursor: pointer;

    @media (min-width: ${props => props.theme.media.phone}) {
      bottom: -1.5rem;
    }
  }
`;

const ImageWrap = styled.div`
  height: 100%;
  overflow: hidden;
  border-radius: 5px;
  box-shadow: ${props =>
    props.theme.mode === "dark"
      ? "10px 10px 14px #181717, -10px -10px 14px #181818"
      : "10px 10px 14px #e1e1e1, -10px -10px 14px #ffffff"};
  transition: transform 0.4s 0.1s linear;
`;

const Box = styled.div`
  ${ThemeBgcAndText};
  border-radius: 0.3rem;
  padding: 1rem;
  margin: 1%;
  display: inline-flex;
  width: 48%;
  height: 15rem;
  transition: box-shadow 0.3s linear;

  &:hover {
    box-shadow: 2px 2px 15px 2px #666;
    & > div :first-child {
      transform: translateY(-2px);
    }
  }

  & > div {
    width: 50%;
    position: relative;
    overflow: hidden;
  }

  @media (max-width: ${props => props.theme.media.phone}) {
    width: 98%;
  }
`;

const Container = styled.div`
  min-height: calc(100vh - var(--nav-height) - var(--footer-height));
  ${ContentWidth};
  margin: 0 auto;
`;
