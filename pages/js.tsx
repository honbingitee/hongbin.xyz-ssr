import { FC, ReactElement, useEffect, useState } from "react";
import { query } from "../api";

interface IProps {}

const JS: FC<IProps> = (): ReactElement => {
  const [inner, setInner] = useState("");

  useEffect(() => {
    query("/getEval").then(({ data }) => {
      if (data) {
        console.log(data);
        setInner(data);
      }
    });
  }, []);

  return (
    <div>
      <script>{inner}</script>
    </div>
  );
};

export default JS;
