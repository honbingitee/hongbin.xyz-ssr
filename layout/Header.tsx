import {
  FC,
  Fragment,
  ReactElement,
  useCallback,
  useRef,
  useState,
  createRef,
  useImperativeHandle,
} from "react";
import styled, { css } from "styled-components";
import {
  Button,
  CSDNIcon as StyledCSDNIcon,
  GithubIcon as StyledGithubIcon,
} from "../components/BUI";
import { ActiveLateY, flexCenter } from "../components/BUI/styled";
import MoonSunSwitch from "../components/BUI/Switch/MoonSunSwitch";
import { ThemeButtonBgc } from "../components/BUI/View/ThemeView";
import {
  SignOutIcon,
  ManagerIcon,
  UserIcon,
  MusicSvg,
} from "../components/BUI/Icon";
import { fadeIn, hideInPhone } from "../components/commentStyled";
import { useAuth } from "../context/AuthProvide";
import { useMount } from "../hooks";
import NavLink from "./NavLink";
import { userProfileRef } from "./UserProfile";
import { useRouter } from "next/router";
import MusicCard, { MusicCardRef } from "../components/MusicCard";

interface IProps {}

export const toggleMenuRef = createRef<{ toggle: () => void }>();

const Head: FC<IProps> = (): ReactElement => {
  const eStyle = useRef<Element | null>(null);
  const eHead = useRef<HTMLHeadElement | null>(null);
  const [openMenu, setOpenMenu] = useState(false);

  useMount(() => {
    eStyle.current = document.createElement("style");
    eHead.current = document.querySelector("head");
  });

  const handleChangeRem = () => {
    if (!eHead.current || !eStyle.current) return;
    eStyle.current.innerHTML = `
      html {
        font-size : 100% !important;
      }
    `;
    eHead.current.appendChild(eStyle.current);
  };

  const handleReductionRem = () => {
    try {
      eStyle?.current && eHead?.current?.removeChild(eStyle.current);
    } catch (_) {
      console.log("已经是青壮年模式了");
    }
  };

  const handleToggleMenu = useCallback(() => {
    setOpenMenu(state => !state);
  }, []);

  useImperativeHandle(toggleMenuRef, () => ({ toggle: handleToggleMenu }), [
    handleToggleMenu,
  ]);

  return (
    <Nav importIndex={openMenu}>
      <NavLink openMenu={openMenu} toggleMenu={handleToggleMenu} />
      <RightOptions>
        <MusicSvg
          onClick={() => {
            MusicCardRef.current?.toggleShow();
          }}
        />
        <MusicCard />
        <Button onClick={handleChangeRem}>关怀模式</Button>
        <Button onClick={handleReductionRem}>青壮年 REM</Button>
        <CSDNIcon />
        <GithubIcon />
        <MoonSunSwitch />
      </RightOptions>
      <HeadPortrait />
    </Nav>
  );
};

export default Head;

const HeadPortrait = () => {
  const { auth, signOut } = useAuth();
  const { push } = useRouter();

  return (
    <HeadPortraitBox
      onClick={() => {
        if (auth.username) return;
        userProfileRef.current && userProfileRef.current.open();
      }}
    >
      {auth.username ? (
        <Fragment>
          <span>{auth.username.slice(0, 2)}</span>
          <PopupBox>
            <div>
              <HeadPortraitItem
                onClick={() => push(`/user/${auth._id}`)}
                title="个人中心"
                icon={<UserIcon />}
              />
              <HeadPortraitItem
                onClick={() =>
                  push("/admin#/admin/accountManage/updateUsername")
                }
                title="改用户名/密码/绑定邮箱 请前往管理端"
                icon={<ManagerIcon />}
              />
              <HeadPortraitItem
                onClick={signOut}
                title="登出"
                icon={<SignOutIcon />}
              />
            </div>
          </PopupBox>
        </Fragment>
      ) : (
        "登录"
      )}
    </HeadPortraitBox>
  );
};

const HeadPortraitItem = ({
  title,
  icon,
  onClick,
}: {
  title: string;
  icon: JSX.Element;
  onClick: () => void;
}) => {
  return (
    <HeadPortraitItemBox onClick={onClick}>
      {icon}
      <span>{title}</span>
    </HeadPortraitItemBox>
  );
};

const HeadPortraitItemBox = styled.div`
  padding: 0.5rem 1rem;
  display: flex;
  align-items: center;
  cursor: pointer;
  transition: background 0.2s linear;
  background-color: #06060665;

  &:hover {
    background: ${props => props.theme.primary};
  }

  & > svg,
  & > svg > path {
    fill: ${props => props.theme.lightText};
    min-width: 1rem;
    min-height: 1rem;
  }
  & > span {
    margin-left: 1rem;
  }
`;

export const filterBackGround = css`
  ${props => props.theme.backdropFilter || props.theme.replaceBDFilter};
  background-color: ${props => props.theme.nav};
`;

const PopupBox = styled.div`
  overflow: hidden;
  position: absolute;
  padding: 0.6rem;
  right: -0.6rem;
  top: 3rem;
  width: 20rem;
  display: none;
  animation: ${fadeIn} 0.3s linear;

  & > div {
    ${filterBackGround};
    overflow: hidden;
    flex: 1;
    border-radius: 3px;
    box-shadow: 0 2px 5px 1px #666;
    display: flex;
    flex-direction: column;
  }
`;

export const HeadPortraitBox = styled.div`
  ${ThemeButtonBgc};
  color: ${props => props.theme.lightText};
  width: 3rem;
  height: 3rem;
  ${flexCenter};
  margin-left: 1rem;
  cursor: pointer;
  transition: box-shadow 0.2s linear;
  border-radius: 3px;
  position: relative;
  ${ActiveLateY};

  & > span {
    transition: transform 0.2s linear;
    :first-letter {
      text-transform: uppercase;
    }
  }

  &:hover {
    box-shadow: 0 0 50px 10px #f9f8f8;
    & > span {
      transform: translateY(-1px);
    }
    ${PopupBox} {
      display: flex;
    }
  }
`;

const RightOptions = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-evenly;
  /* background-color: #6452ad57; */
  border: 0.0625rem dashed
    ${props => (props.theme.mode == "dark" ? "#ccc" : "#fff")};
  height: 100%;
  padding: 0 0.1rem;
  cursor: default;

  @media (max-width: ${props => props.theme.media.phone}) {
    border: none;
  }

  & > * {
    margin-left: 0.4rem;
    margin-right: 0.4rem;
  }

  button,
  a {
    ${hideInPhone};
  }

  svg {
    width: 2rem;
    height: 2rem;
    cursor: pointer;
  }
`;

const Nav = styled.nav<{ importIndex: boolean }>`
  position: sticky;
  position: -webkit-sticky;
  /* position: fixed; */
  top: 0;
  z-index: ${props => props.theme.index[props.importIndex ? "max" : "fixed"]};
  width: 100%;
  height: var(--nav-height);
  max-height: var(--nav-height);
  padding: 0.5rem 1rem;
  display: flex;
  align-items: center;
  justify-content: space-between;
  ${filterBackGround};
  user-select: none;
`;

export const GithubIcon = () => (
  <a href="https://github.com/Jedi-hongbin" target="_black">
    <StyledGithubIcon />
  </a>
);

export const CSDNIcon = () => (
  <a href="https://blog.csdn.net/printf_hello" target="_black">
    <StyledCSDNIcon />
  </a>
);
