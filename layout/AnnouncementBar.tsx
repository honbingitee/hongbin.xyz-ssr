/*
 * @Author: 宏斌
 * @Date: 2021-09-13 21:10:22
 * @LastEditTime: 2021-10-01 17:24:42
 * @Description: 公告栏  http 访问时 推荐 https 访问
 * @FilePath: /hongbin_xyz_web/layout/AnnouncementBar.tsx
 */
import { FC, ReactElement, useEffect, useState } from "react";
import styled, { css } from "styled-components";
import { flexCenter } from "../components/BUI/styled";
import { ThemePrimaryBgc } from "../components/BUI/View/ThemeView";
import { CancelIcon } from "../components/BUI/Icon";

interface IProps {}
//TODO  获取后端的活动/推荐信息 展示
const AnnouncementBar: FC<IProps> = (): ReactElement => {
  const [isHttp, setIsHttp] = useState(false); // 当前是否为http访问
  const [hide, setHide] = useState(true); // 是否隐藏
  useEffect(() => {
    const isHttp = window.location.protocol === "http:";
    setIsHttp(isHttp);
    setHide(!isHttp);
  }, []);

  return (
    <Container hide={hide}>
      {isHttp ? (
        <a href="https://hongbin.xyz">支持 https 安全访问 点击前往</a>
      ) : null}
      <div onClick={() => setHide(true)}>
        <CancelIcon />
      </div>
    </Container>
  );
};

export default AnnouncementBar;

const Container = styled.div<{ hide: boolean }>`
  ${ThemePrimaryBgc};
  transition: height 0.3s linear, opacity 0.3s linear, padding 0.3s linear;
  padding: 0.5rem;
  color: ${props => props.theme.lightText};
  ${flexCenter};
  position: relative;
  ${props =>
    props.hide &&
    css`
      opacity: 0;
      height: 0;
      padding: 0;
    `};

  & > div {
    position: absolute;
    right: 1rem;
    cursor: pointer;
    transition: transform 0.3s linear;
    ${flexCenter};
    &:hover {
      transform: rotate(90deg);
    }
  }
`;
