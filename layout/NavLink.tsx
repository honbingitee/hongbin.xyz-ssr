import { FC, ReactElement, useEffect, useState } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import styled, { css } from "styled-components";
import { CancelIcon, MenuIcon } from "../components/BUI/Icon";
import { leftSlideIn, showInPhone, fadeIn } from "../components/commentStyled";
import { flexCenter } from "../components/BUI/styled";
import { CSDNIcon, GithubIcon } from "./Header";

interface IProps {
  openMenu: boolean;
  toggleMenu: () => void;
}

const NavLink: FC<IProps> = ({ openMenu, toggleMenu }): ReactElement => {
  const { pathname } = useRouter();
  const [prevPath, setPrevPath] = useState(pathname);

  useEffect(() => {
    if (document.body.clientWidth < 750 && openMenu) {
      console.log("prevPath:", prevPath);
      if (prevPath !== pathname) toggleMenu();
      setPrevPath(pathname);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [pathname, openMenu]);

  return (
    <Container>
      <LinkContent onClick={toggleMenu} active={false}>
        <MenuIcon />
      </LinkContent>
      <LinkWrap openMenu={openMenu}>
        <Link href="/" passHref>
          <LinkContent active={pathname == "/"}>主页</LinkContent>
        </Link>
        <Link href="/todoList" passHref>
          <LinkContent active={pathname == "/todoList"}>代办事项</LinkContent>
        </Link>
        <Link href="/blogList" passHref>
          <LinkContent active={pathname == "/blogList"}>博文列表</LinkContent>
        </Link>
        <MoreLinkItem active={false}>
          <span>更多...</span>
          {/* 不直接展出 hover“更多”后以menuItem出现 */}
          <div>
            <Link href="/tagCanvas" passHref>
              <LinkContent active={pathname == "/tagCanvas"}>
                标签画板
              </LinkContent>
            </Link>
            <Link href="/treasureChest" passHref>
              <LinkContent active={pathname == "/treasureChest"}>
                百宝箱
              </LinkContent>
            </Link>
            <Link href="/sourceSquare" passHref>
              <LinkContent active={pathname == "/sourceSquare"}>
                资源广场
              </LinkContent>
            </Link>
            <Link href="/resume" passHref>
              <LinkContent active={pathname == "/resume"}>我的简历</LinkContent>
            </Link>
            <Link href="/blog/613c825ec3518d54ad76bf99" passHref>
              <LinkContent active={false}>关于本站</LinkContent>
            </Link>
          </div>
        </MoreLinkItem>
        <OutLinkBox>
          <CSDNIcon />
          <GithubIcon />
        </OutLinkBox>
        <div data-cancel-button onClick={toggleMenu}>
          <CancelIcon />
        </div>
      </LinkWrap>
      <DrawerMask onClick={toggleMenu} />
    </Container>
  );
};

export default NavLink;

const OutLinkBox = styled.div`
  display: none;
  justify-content: space-around;
  ${showInPhone};
  position: absolute;
  bottom: 0;

  & > a {
    padding: 1rem;
    padding-left: 0;
  }
`;

const mediaOpen = css`
  left: 0;
  display: flex !important;
  & > div[data-cancel-button] {
    background: inherit;
    border-radius: 100%;
    width: 3rem;
    height: 3rem;
    ${flexCenter};
    box-shadow: 0 0 10px 1px #5c5c5c8f;
    position: absolute;
    bottom: 10rem;
    left: 50%;
  }
  & + div {
    display: block;
    animation: ${fadeIn} 0.3s linear;
  }
`;

const linearBackground = css`
  ${props =>
    props.theme.mode === "dark"
      ? `
      background: linear-gradient(
            60deg,
            rgb(0, 0, 0),
            rgba(55, 55, 55, 0.801)
          );
      `
      : `
      background: linear-gradient(
          60deg,
          rgb(0, 195, 255),
          rgba(204, 0, 255) 90%
        );
`}
`;

const LinkWrap = styled.div<{ openMenu: boolean }>`
  padding: 0 1rem;
  height: 100%;

  & > div[data-cancel-button] {
    display: none;
  }

  @media (max-width: ${props => props.theme.media.phone}) {
    position: fixed;
    animation: ${leftSlideIn} 0.3s linear;
    top: 0;
    display: none;
    width: 80vw;
    height: 100vh;
    ${linearBackground};
    flex-direction: column;
    z-index: ${props => props.theme.index.max};
    /* border-bottom-right-radius: 100%; */
    & > div {
      margin-top: 1.5rem;
    }
    /* &::before {
      content: "";
      position: inherit;
      width: inherit;
      height: inherit;
      background: #fff;
      top: 0;
      left: 0;
      z-index: -3;
    } */
  }

  ${props => props.openMenu && mediaOpen};
`;

const DrawerMask = styled.div`
  display: none;
  position: fixed;
  width: 100vw;
  height: 100vh;
  top: 0;
  left: 0;
  z-index: 10;
  background: #0000007f;
`;

const LinkContent = styled.div<{ active: boolean }>`
  padding: 0 1rem;
  text-decoration: none;
  color: ${props => props.theme?.lightText};
  text-shadow: 1px 1px 10px #000;
  letter-spacing: 2px;
  height: 100%;
  max-height: 3rem;
  display: inline-flex;
  align-items: center;
  background-color: ${({ theme }) =>
    theme?.mode == "dark" ? "#ffffff56" : "#4e464658"};
  margin: 0 0.2rem;
  /* transition: all 0.3s ease-in; */
  transition-property: box-shadow, background-color, border-radius;
  transition-timing-function: ease-in;
  transition-duration: 0.3s;
  cursor: pointer;

  ${props =>
    props.active
      ? `box-shadow: 0 0 50px 10px #f9f8f8;
         border-radius: 3px;`
      : undefined}
  :hover {
    background-color: #06060665;
    box-shadow: 0 0 50px 2px #f9f8f8;
    border-radius: 3px;
  }
`;

const MoreLinkItem = styled(LinkContent)`
  position: relative;
  width: 7rem;
  & > div {
    flex-direction: column;
    position: absolute;
    top: 3rem;
    left: 0;
    display: none;
    padding-top: 0.5rem;

    & > div {
      height: 3rem;
      margin: 0.2rem 0;
    }
  }

  :hover {
    & > div {
      display: flex;
    }
  }

  @media (max-width: ${props => props.theme.media.phone}) {
    width: auto;
    height: 0;
    text-shadow: none;
    color: transparent;
    margin-top: 0 !important;
    & > div {
      display: flex;
      width: 100%;
      transform: translateY(-3.5rem);
      & > div {
        margin-top: 1.5rem;
      }
    }
  }
`;

const Container = styled.div`
  height: 100%;
  display: flex;
  flex: 1;
  position: relative;

  & > div:first-child {
    display: none;
    ${showInPhone};
  }
`;
