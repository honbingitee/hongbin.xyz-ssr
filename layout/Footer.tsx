import { FC, ReactElement } from "react";
import Image from "next/image";
import styled from "styled-components";
import footerPng from "../assets/footer.jpg";
import guohui from "../assets/guohui.png";
import { AbsoluteFillView } from "../components/BUI";

interface IProps {}
//TODO 点赞/踩   进入页面增加阅读了
const Footer: FC<IProps> = (): ReactElement => {
  return (
    <Container>
      <Image src={footerPng} alt="Footer" layout="fill" objectFit="cover" />
      <AbsoluteFillObject>
        <Title>前面的区域、以后再来探索吧</Title>
        <Info>
          <Image width="20px" height="20px" src={guohui} alt="guohui" />
          <span>辽ICP备20005121号-1</span>
        </Info>
      </AbsoluteFillObject>
    </Container>
  );
};

export default Footer;

const Info = styled.div`
  display: flex;
  align-items: center;
  position: absolute;
  bottom: 10px;
  color: ${props => props.theme?.second};
  span {
    margin-left: 5px;
  }
`;

const Title = styled.span`
  font-size: 3rem;
  text-shadow: 4px 5px 8px #000;
  background: linear-gradient(34deg, #05f9f1, #ffffff);
  color: transparent;
  -webkit-background-clip: text;
  background-clip: text;
  opacity: 0.9;

  @media screen and (max-width: ${props => props.theme.media.phone}) {
    font-size: 2rem;
  }
`;

const AbsoluteFillObject = styled(AbsoluteFillView)`
  display: flex;
  justify-content: space-around;
  align-items: center;
  flex-direction: column;
`;

const Container = styled.div`
  width: inherit;
  height: var(--footer-height);
  overflow: hidden;
  position: relative;
  ${props => props.theme.mode === "dark" && "filter: brightness(0.5);"};
  @media (max-width: ${props => props.theme.media.phone}) {
    height: 10rem;
  }
`;
