import {
  FC,
  ReactElement,
  useState,
  createRef,
  useImperativeHandle,
  useCallback,
  useEffect,
  useRef,
} from "react";
import styled, { keyframes } from "styled-components";
import { flexCenter } from "../components/BUI/styled";
import { ThemeBgc } from "../components/BUI/View/ThemeView";
import { fadeIn } from "../components/commentStyled";
import {
  HelloIcon,
  CarIcon,
  RightIcon,
  CancelIcon,
} from "../components/BUI/Icon";
import EllipseLabelInput from "../components/BUI/Input/EllipseLabelInput";
import { Button, FlexDiv } from "../components/BUI";
import { checkUsernameValid } from "../api/authApi";
import LoadIcon from "../components/BUI/Icon/LoadIcon";
import { useAuth } from "../context/AuthProvide";
import RASEncrypt from "../utils/encrypt";

interface IProps {}

const initialErrors = { username: false, password: false };

export const userProfileRef = createRef<{ open: () => void }>();

const UserProfile: FC<IProps> = (): ReactElement => {
  const { login, register } = useAuth();
  const [show, setShow] = useState<boolean>(false);
  const [isLogin, setIsLogin] = useState(true);
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [errors, setErrors] = useState(initialErrors);
  const [loading, setLoading] = useState(false);
  const mask = useRef<HTMLDivElement>(null);

  // 注册时验证用户名是否已注册
  useEffect(() => {
    if (!isLogin && username) {
      checkUsernameValid(username).then(res => {
        console.log("confirm:", res);
        setErrors(prev => ({ ...prev, username: !res }));
      });
    } else if (isLogin && username) {
      setErrors(prev => ({ ...prev, username: !username }));
    }
  }, [isLogin, username]);

  useEffect(() => {
    if (isLogin && password) {
      setErrors(prev => ({ ...prev, password: !password }));
    }
  }, [isLogin, password]);

  const listenKeyEvent = (e: KeyboardEvent) => {
    e.code === "Escape" && setShow(false);
  };

  //esc 键监听 keyCode 已被废弃
  useEffect(() => {
    if (show) {
      if (mask.current) {
        mask.current.style["display"] = "flex";
      }
      window.addEventListener("keyup", listenKeyEvent);
      return () => {
        window.removeEventListener("keyup", listenKeyEvent);
      };
    } else {
      setTimeout(() => {
        if (mask.current) {
          mask.current.style["display"] = "none";
        }
      }, 300);
    }
  }, [show]);

  const open = useCallback(() => {
    setShow(true);
  }, []);

  useImperativeHandle(userProfileRef, () => ({ open }), [open]);

  const handleSubmit = async () => {
    //step 1 拥有username password
    if (!username || !password)
      return setErrors({ username: !username, password: !password });
    //step 2 没有error
    if (errors.username && errors.password) return;
    const enPassword = RASEncrypt().encrypt(password);
    const user = { username, password: enPassword };
    setLoading(true);
    const func = isLogin ? login : register;
    const result = await func(user);
    setLoading(false);
    if (result && result.message) {
      return setShow(false);
    }
    setErrors(props => ({ ...props, password: true }));
  };

  return (
    <Mask ref={mask} show={show}>
      <div>
        <CancelBox onClick={() => setShow(false)}>
          <CancelIcon />
        </CancelBox>
        <CarIcon />
        <HelloBox>
          <HelloIcon />
          <span>HELLO</span>
        </HelloBox>
        <MainBox isLogin={isLogin}>
          <EllipseLabelInput
            label="用户名"
            value={username}
            setValue={setUsername}
            error={errors.username}
            errorText="用户名不可用"
            maxLength={20}
          />
          <EllipseLabelInput
            label="密码"
            value={password}
            setValue={setPassword}
            type="password"
            error={errors.password}
            errorText="密码错误"
          />
        </MainBox>

        <FlexDiv items="center">
          <Button
            style={{ margin: "0.5rem 0.5rem 0.5rem 0", flex: 1 }}
            onClick={handleSubmit}
            justify="center"
          >
            {loading ? <LoadIcon /> : null}
            {isLogin ? "登陆" : "注册"}
          </Button>
          <Button
            onClick={() => {
              setIsLogin(state => !state);
              setUsername("");
              setPassword("");
              setErrors(() => initialErrors);
            }}
          >
            {isLogin ? "注册" : "登陆"}
            <RightIcon />
          </Button>
        </FlexDiv>
      </div>
    </Mask>
  );
};

export default UserProfile;

const CancelBox = styled.div`
  position: absolute;
  top: -1rem;
  right: -1rem;
  width: 2rem;
  height: 2rem;
  border-radius: 1rem;
  ${ThemeBgc};
  ${flexCenter};
  transition: transform 0.3s linear;
  z-index: 1; //小飞机覆盖不了他
  cursor: pointer;

  &:hover {
    transform: scale(1.5) rotate(90deg);
  }

  & > svg > path {
    fill: ${props => props.theme.fg};
  }
`;

const MainBox = styled.div<{ isLogin: boolean }>`
  margin-top: 1rem;
  transition: transform 0.5s ease;

  ${props => (props.isLogin ? undefined : `transform:rotateX(360deg) `)};
`;

const HelloBox = styled.div`
  font-weight: bold;
  width: 100%;
  height: 10rem;
  background: ${props => props.theme.button.bg};
  line-height: 10rem;
  font-size: 2rem;
  border-radius: 5px;
  color: #fff;
  user-select: none;
  letter-spacing: 3px;
  display: flex;
  align-items: center;
  text-align: center;

  & > span {
    flex: 1;
  }

  & > svg {
    width: 9rem;
    height: 9rem;
  }
`;

const topAnimate = keyframes`
    0%{
        top:0;
        right: -160px;
        transform: rotate(90deg);
    }
    20%{
        top:calc(100% - 80px);
        right: -160px;
        transform: rotate(90deg);
    }
    24%{
        top:100%;
        right: -160px;
        transform: rotate(180deg);
    }
    44%{
        top:100%;
        right: calc(100% - 80px);
        transform: rotate(180deg);
    }
    49%{
        top:100%;
        right: 100%;
        transform: rotate(270deg);
    }
    68%{
        top:-80px;
        right: 100%;
        transform: rotate(270deg);
    }
    72%{
        top:-160px;
        right: 100%;
        transform: rotate(360deg);
    }
    92%{
        top:-160px;
        right: -80px;
        transform: rotate(360deg);
    }
    95%{
        top:-160px;
        right: -160px;
        transform: rotate(450deg);
    }
    100%{
        top:0;
        right: -160px;
        transform: rotate(450deg);
    }
`;

const Mask = styled.div<{ show: boolean }>`
  ${props => props.theme.backdropFilter || props.theme.replaceBDFilter};
  position: fixed;
  width: 100vw;
  height: 100vh;
  z-index: ${props => props.theme.index.max};
  top: 0;
  left: 0;
  ${flexCenter};
  display: none;
  animation: ${fadeIn} 0.3s linear;
  transition: opacity 0.3s linear;
  opacity: ${props => (props.show ? 1 : 0)};

  & > div {
    width: 20rem;
    box-shadow: 0 0 5px 0 #ccc;
    padding: 0.5rem;
    ${ThemeBgc};
    border-radius: 5px;
    position: relative;

    & > svg {
      position: absolute;
      animation: ${topAnimate} 8s linear infinite;
    }
  }
`;
