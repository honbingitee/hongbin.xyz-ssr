import { FC, ReactElement } from "react";
import NextHead from "next/head";
import { NextSeo, NextSeoProps } from "next-seo";

interface IProps extends NextSeoProps {
  title: string;
  image?: string;
  logo?: string; //网站标签页的logo路径
}
/**
 * @description: seo功能
 * @param  { title,description,image,...}
 * @return {ReactElement}
 */
const SEO: FC<IProps> = ({
  title,
  // description = "欢迎访问宏斌的博客.png",
  description = "JUST DO IT",
  image,
  logo,
  ...props
}): ReactElement => {
  return (
    <>
      <NextSeo
        title={title}
        description={description}
        openGraph={{
          url: "https://hongbin.xyz/",
          title: title,
          description: description,
          images: [
            {
              url:
                image || "https://api.hongbin.xyz:3002/public/images/web.jpg",
              width: 200,
              height: 200,
              alt: "Og Image Alt",
            },
          ],
          site_name: "hello",
        }}
        {...props}
      />
      <NextHead>
        <link rel="icon" href={logo || "/favicon.svg"} />
      </NextHead>
    </>
  );
  return (
    <NextHead>
      <title>{title}</title>
      <meta name="description" content={`this is ${title} page`} />
      <link rel="icon" href="/favicon.ico" />
    </NextHead>
  );
};

export default SEO;
