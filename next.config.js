module.exports = {
  reactStrictMode: true,
  env: {
    NEXT_PUBLIC_DEV_URL: "http://localhost:3002"
  },
  images: {
    domains: ['api.hongbin.xyz:3002', 'api.hongbin.xyz', 'hongbin.xyz:3002', "localhost", 'localhost:3002', 'hongbin.xyz']
  }
}
