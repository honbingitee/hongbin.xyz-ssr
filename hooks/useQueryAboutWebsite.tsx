import { useEffect, useState } from "react";
import { getAboutWebsite } from "../api/blogApi";

const useQueryAboutWebsite = (): [string, boolean] => {
  const [content, setContent] = useState("");
  const [err, setErr] = useState(false);

  useEffect(() => {
    getAboutWebsite().then(res => {
      console.log(res.data);
      if (res.data && res.data.code === 0) {
        return setContent(res.data.content);
      }
      setErr(true);
    });
  }, []);

  return [content, err];
};

export default useQueryAboutWebsite;
