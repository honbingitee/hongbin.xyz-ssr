import { useEffect } from "react";

interface fun {
  (): void | any;
}

const useMount = (callback: fun) => {
  useEffect(callback, []);
};

export default useMount;
