// export const baseURL = "http://localhost:3002";
export const baseURL = "https://api.hongbin.xyz:3002";
export const getImagePath = baseURL + "/getImage/";
export const downloadSourcePath = baseURL + "/downloadSource/";
export const getMusicPath = baseURL + "/getMusic/";
