/**
 * @description: 开启拖拽
 * @param {*} selectors 容器选择器
 * @param {*} selectors 子元素视觉差选择器
 */

const addDrag = (selectors, childSelect) => {
    const containers = document.querySelectorAll(selectors);
    const angle = 15;

    for (const container of containers) {
        const child = container.querySelectorAll(childSelect);

        container.addEventListener('mouseenter', () => {
            container.style.transition = '';
            container.style['will-change'] = "transform";
            child.forEach(element => {
                element.style['will-change'] = "transform";
                element.style.transition = '';
            });
        })
        container.addEventListener('mousemove', ({ pageX, pageY }) => {
            // 设置获取card中心点坐标
            const { offsetWidth, offsetHeight, offsetLeft, offsetTop } = container;
            const halfW = offsetWidth / 2;
            const halfH = offsetHeight / 2;
            const centerX = offsetLeft + halfW;
            const centerY = offsetTop + halfH;
            const dirX = pageX - centerX;
            const dirY = pageY - centerY;

            container.style.transform = `perspective(1000px)  rotateX(${dirX / halfW * angle}deg) rotateY(${dirY / halfH * angle}deg)`;
            child.forEach(element => {
                element.style.transform = `perspective(500px)  rotateY(${dirX / halfW * -angle}deg)`;
            });
        })
        container.addEventListener('mouseleave', () => {
            // 复原
            container.style.transform = '';
            container.style.transition = 'transform 0.3s linear';
            child.forEach(element => {
                element.style.transform = '';
                element.style.transition = 'transform 0.3s linear';
            });
        })
    }
}

export default addDrag;