export interface ASCIIProps {
  b: string;
  dec: number;
  hex: string;
  abbr?: string;
  explain?: string;
  char?: string;
}

export const ASCIIDescription = {
  desc: "ASCII ((American Standard Code for Information Interchange): 美国信息交换标准代码）是基于拉丁字母的一套电脑编码系统，主要用于显示现代英语和其他西欧语言。它是最通用的信息交换标准，并等同于国际标准ISO/IEC 646。ASCII第一次以规范标准的类型发表是在1967年，最后一次更新则是在1986年，到目前为止共定义了128个字符",
  link: "https://baike.baidu.com/item/ASCII",
};

/**
ASCII码数组
@params  b: string; 二进制
@params  dec: number; 十进制
@params  hex: string; 十六进制
@params  abbr?: string; 缩写
@params  explain?: string; 解释
@params  char?: string; 可显示字符
 */
const ASCIIS: ASCIIProps[] = [
  { b: "00000000", dec: 0, hex: "00", abbr: "NUL (NULL)", explain: "空字符" },
  {
    b: "00000001",
    dec: 1,
    hex: "01",
    abbr: "SOH (Start Of Headling)",
    explain: "标题开始",
  },
  {
    b: "00000010",
    dec: 2,
    hex: "02",
    abbr: "STX (Start Of Text)",
    explain: "正文开始",
  },
  {
    b: "00000011",
    dec: 3,
    hex: "03",
    abbr: "ETX (End Of Text)",
    explain: "正文结束",
  },
  {
    b: "00000100",
    dec: 4,
    hex: "04",
    abbr: "EOT (End Of Transmission)",
    explain: "传输结束",
  },
  { b: "00000101", dec: 5, hex: "05", abbr: "ENQ (Enquiry)", explain: "请求" },
  {
    b: "00000110",
    dec: 6,
    hex: "06",
    abbr: "ACK (Acknowledge)	回应/响应/",
    explain: "收到通知",
  },
  { b: "00000111", dec: 7, hex: "07", abbr: "BEL (Bell)	", explain: "响铃" },
  { b: "00001000", dec: 8, hex: "08", abbr: "BS (Backspace)	", explain: "退格" },
  {
    b: "00001001",
    dec: 9,
    hex: "09",
    abbr: "HT (Horizontal Tab)",
    explain: "水平制表符",
  },
  {
    b: "00001010",
    dec: 10,
    hex: "0A",
    abbr: "LF/NL(Line Feed/New Line)",
    explain: "换行键",
  },
  {
    b: "00001011",
    dec: 11,
    hex: "0B",
    abbr: "VT (Vertical Tab)	",
    explain: "垂直制表符",
  },
  {
    b: "00001100",
    dec: 12,
    hex: "0C",
    abbr: "FF/NP (Form Feed/New Page)",
    explain: "换页键",
  },
  {
    b: "00001101",
    dec: 13,
    hex: "0D",
    abbr: "CR (Carriage Return)",
    explain: "回车键",
  },
  {
    b: "00001110",
    dec: 14,
    hex: "0E",
    abbr: "SO (Shift Out)",
    explain: "不用切换",
  },
  {
    b: "00001111",
    dec: 15,
    hex: "0F",
    abbr: "SI (Shift In)",
    explain: "启用切换",
  },
  {
    b: "00010000",
    dec: 16,
    hex: "10",
    abbr: "DLE (Data Link Escape)",
    explain: "数据链路转义",
  },
  {
    b: "00010001",
    dec: 17,
    hex: "11",
    abbr: "DC1/XON (Device Control 1/Transmission On)",
    explain: "设备控制1/传输开始",
  },
  {
    b: "00010010",
    dec: 18,
    hex: "12",
    abbr: "DC2 (Device Control 2)",
    explain: "设备控制2",
  },
  {
    b: "00010011",
    dec: 19,
    hex: "13",
    abbr: "DC3/XOFF (Device Control 3/Transmission Off)",
    explain: "设备控制3/传输中断",
  },
  {
    b: "00010100",
    dec: 20,
    hex: "14",
    abbr: "DC4 (Device Control 4)",
    explain: "设备控制4",
  },
  {
    b: "00010101",
    dec: 21,
    hex: "15",
    abbr: "NAK (Negative Acknowledge)",
    explain: "无响应/非正常响应/拒绝接收",
  },
  {
    b: "00010110",
    dec: 22,
    hex: "16",
    abbr: "SYN (Synchronous Idle)",
    explain: "同步空闲",
  },
  {
    b: "00010111",
    dec: 23,
    hex: "17",
    abbr: "ETB (End of Transmission Block)",
    explain: "传输块结束/块传输终止",
  },
  { b: "00011000", dec: 24, hex: "18", abbr: "CAN (Cancel)", explain: "取消" },
  {
    b: "00011001",
    dec: 25,
    hex: "19",
    abbr: "EM (End of Medium)",
    explain: "已到介质末端/介质存储已满/介质中断",
  },
  {
    b: "00011010",
    dec: 26,
    hex: "1A",
    abbr: "SUB (Substitute)",
    explain: "替补/替换",
  },
  {
    b: "00011011",
    dec: 27,
    hex: "1B",
    abbr: "ESC (Escape)",
    explain: "逃离/取消",
  },
  {
    b: "00011100",
    dec: 28,
    hex: "1C",
    abbr: "FS (File Separator)",
    explain: "文件分割符",
  },
  {
    b: "00011101",
    dec: 29,
    hex: "1D",
    abbr: "GS (Group Separator)",
    explain: "组分隔符/分组符",
  },
  {
    b: "00011110",
    dec: 30,
    hex: "1E",
    abbr: "RS (Record Separator)",
    explain: "记录分离符",
  },
  {
    b: "00011111",
    dec: 31,
    hex: "1F",
    abbr: "US (Unit Separator)",
    explain: "单元分隔符",
  },
  { b: "00100000", dec: 32, hex: "20", abbr: "(Space)", explain: "空格" },
  { b: "00100001", dec: 33, hex: "21", char: "!" },
  { b: "00100010", dec: 34, hex: "22", char: '"' },
  { b: "00100011", dec: 35, hex: "23", char: "#" },
  { b: "00100100", dec: 36, hex: "24", char: "$" },
  { b: "00100101", dec: 37, hex: "25", char: "%" },
  { b: "00100110", dec: 38, hex: "26", char: "&" },
  { b: "00100111", dec: 39, hex: "27", char: "'" },
  { b: "00101000", dec: 40, hex: "28", char: "(" },
  { b: "00101001", dec: 41, hex: "29", char: ")" },
  { b: "00101010", dec: 42, hex: "2A", char: "*" },
  { b: "00101011", dec: 43, hex: "2B", char: "+" },
  { b: "00101100", dec: 44, hex: "2C", char: "," },
  { b: "00101101", dec: 45, hex: "2D", char: "-" },
  { b: "00101110", dec: 46, hex: "2E", char: "." },
  { b: "00101111", dec: 47, hex: "2F", char: "/" },
  { b: "00110000", dec: 48, hex: "30", char: "0" },
  { b: "00110001", dec: 49, hex: "31", char: "1" },
  { b: "00110010", dec: 50, hex: "32", char: "2" },
  { b: "00110011", dec: 51, hex: "33", char: "3" },
  { b: "00110100", dec: 52, hex: "34", char: "4" },
  { b: "00110101", dec: 53, hex: "35", char: "5" },
  { b: "00110110", dec: 54, hex: "36", char: "6" },
  { b: "00110111", dec: 55, hex: "37", char: "7" },
  { b: "00111000", dec: 56, hex: "38", char: "8" },
  { b: "00111001", dec: 57, hex: "39", char: "9" },
  { b: "00111010", dec: 58, hex: "3A", char: ":" },
  { b: "00111011", dec: 59, hex: "3B", char: ";" },
  { b: "00111100", dec: 60, hex: "3C", char: "<" },
  { b: "00111101", dec: 61, hex: "3D", char: "=" },
  { b: "00111110", dec: 62, hex: "3E", char: ">" },
  { b: "00111111", dec: 63, hex: "3F", char: "?" },
  { b: "01000000", dec: 64, hex: "40", char: "@" },
  { b: "01000001", dec: 65, hex: "41", char: "A" },
  { b: "01000010", dec: 66, hex: "42", char: "B" },
  { b: "01000011", dec: 67, hex: "43", char: "C" },
  { b: "01000100", dec: 68, hex: "44", char: "D" },
  { b: "01000101", dec: 69, hex: "45", char: "E" },
  { b: "01000110", dec: 70, hex: "46", char: "F" },
  { b: "01000111", dec: 71, hex: "47", char: "G" },
  { b: "01001000", dec: 72, hex: "48", char: "H" },
  { b: "01001001", dec: 73, hex: "49", char: "I" },
  { b: "01001010", dec: 74, hex: "4A", char: "J" },
  { b: "01001011", dec: 75, hex: "4B", char: "K" },
  { b: "01001100", dec: 76, hex: "4C", char: "L" },
  { b: "01001101", dec: 77, hex: "4D", char: "M" },
  { b: "01001110", dec: 78, hex: "4E", char: "N" },
  { b: "01001111", dec: 79, hex: "4F", char: "O" },
  { b: "01010000", dec: 80, hex: "50", char: "P" },
  { b: "01010001", dec: 81, hex: "51", char: "Q" },
  { b: "01010010", dec: 82, hex: "52", char: "R" },
  { b: "01010011", dec: 83, hex: "53", char: "S" },
  { b: "01010100", dec: 84, hex: "54", char: "T" },
  { b: "01010101", dec: 85, hex: "55", char: "U" },
  { b: "01010110", dec: 86, hex: "56", char: "V" },
  { b: "01010111", dec: 87, hex: "57", char: "W" },
  { b: "01011000", dec: 88, hex: "58", char: "X" },
  { b: "01011001", dec: 89, hex: "59", char: "Y" },
  { b: "01011010", dec: 90, hex: "5A", char: "Z" },
  { b: "01011011", dec: 91, hex: "5B", char: "[" },
  { b: "01011100", dec: 92, hex: "5C", char: "\\" },
  { b: "01011101", dec: 93, hex: "5D", char: "]" },
  { b: "01011110", dec: 94, hex: "5E", char: "^" },
  { b: "01011111", dec: 95, hex: "5F", char: "_" },
  { b: "01100000", dec: 96, hex: "60", char: "`" },
  { b: "01100001", dec: 97, hex: "61", char: "a" },
  { b: "01100010", dec: 98, hex: "62", char: "b" },
  { b: "01100011", dec: 99, hex: "63", char: "c" },
  { b: "01100100", dec: 100, hex: "64", char: "d" },
  { b: "01100101", dec: 101, hex: "65", char: "e" },
  { b: "01100110", dec: 102, hex: "66", char: "f" },
  { b: "01100111", dec: 103, hex: "67", char: "g" },
  { b: "01101000", dec: 104, hex: "68", char: "h" },
  { b: "01101001", dec: 105, hex: "69", char: "i" },
  { b: "01101010", dec: 106, hex: "6A", char: "j" },
  { b: "01101011", dec: 107, hex: "6B", char: "k" },
  { b: "01101100", dec: 108, hex: "6C", char: "l" },
  { b: "01101101", dec: 109, hex: "6D", char: "m" },
  { b: "01101110", dec: 110, hex: "6E", char: "n" },
  { b: "01101111", dec: 111, hex: "6F", char: "o" },
  { b: "01110000", dec: 112, hex: "70", char: "p" },
  { b: "01110001", dec: 113, hex: "71", char: "q" },
  { b: "01110010", dec: 114, hex: "72", char: "r" },
  { b: "01110011", dec: 115, hex: "73", char: "s" },
  { b: "01110100", dec: 116, hex: "74", char: "t" },
  { b: "01110101", dec: 117, hex: "75", char: "u" },
  { b: "01110110", dec: 118, hex: "76", char: "v" },
  { b: "01110111", dec: 119, hex: "77", char: "w" },
  { b: "01111000", dec: 120, hex: "78", char: "x" },
  { b: "01111001", dec: 121, hex: "79", char: "y" },
  { b: "01111010", dec: 122, hex: "7A", char: "z" },
  { b: "01111011", dec: 123, hex: "7B", char: "{" },
  { b: "01111100", dec: 124, hex: "7C", char: "|" },
  { b: "01111101", dec: 125, hex: "7D", char: "}" },
  { b: "01111110", dec: 126, hex: "7E", char: "~" },
  { b: "01111111", dec: 127, hex: "7F", abbr: "DEL (Delete)", explain: "删除" },
];
export default ASCIIS;
