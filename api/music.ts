/*
 * @Author: hongbin
 * @Date: 2021-12-10 17:04:47
 * @LastEditors: hongbin
 * @LastEditTime: 2021-12-10 17:22:55
 * @Description:音乐
 */

import { query } from ".";
import { RequestBack } from "./types";

export interface IMusicInfo {
  name: string;
  link: string; // URL
  desc: string;
  cover: string;
}

export const getMusicInfo = (): RequestBack<IMusicInfo> =>
  query("getMusicInfo");
