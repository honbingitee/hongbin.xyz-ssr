import { get } from ".";

export const getPokers = () => {
  return get("/getPokers");
};
