/*
 * @Author: your name
 * @Date: 2021-10-15 15:22:27
 * @LastEditTime: 2021-10-20 11:18:45
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: /hongbin_xyz_web/api/timeTaskApi.ts
 */

import { request } from ".";
import { RequestBack } from "./types";

interface ICreateTimeTask {
  content: string;
  interval: string;
  mail: string;
}

export const createTimeTask = (params: ICreateTimeTask) =>
  request("/createTimeTask", params);

interface IQueryTimeTask {
  mail: string;
}

export interface ITask extends ICreateTimeTask {
  _id: string;
}

export const queryTimeTaskByMail = (
  params: IQueryTimeTask
): RequestBack<{ message: string; list: ITask[] }> =>
  request("/queryTimeTaskByMail", params);

export const deleteTimeTask = (id: string) =>
  request("/deleteTimeTask", { id });

interface IUpdateTimeTask extends Omit<ICreateTimeTask, "mail"> {
  _id: string;
}

export const updateTimeTask = (params: IUpdateTimeTask) =>
  request("/updateTimeTask", params);
