import { query } from "./index";

export interface ITreasure {
  _id: string;
  name: string;
  images: string[];
  description: string;
  url: string;
}

export const queryTreasureChest = (): Promise<{
  data: { treasures: ITreasure[] };
}> => query("/queryTreasureChest");
