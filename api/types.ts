/*
 * @Author: 宏斌
 * @Date: 2021-08-24 22:22:26
 * @LastEditTime: 2021-09-19 14:22:02
 * @LastEditors: Please set LastEditors
 * @Description: 供web端和管理端使用
 * @FilePath: /hongbin_xyz_admin/src/api/types.tsx
 */

// 全部 公开  私密  草稿
export enum BlogStatus {
  ALL = "全部",
  PUBLIC = "公开",
  PRIVATE = "私密",
  DRAFT = "草稿",
}
//原创 or  转载
export enum BlogType {
  ORIGINAL = "原创",
  REPRINT = "转载",
}
export interface BlogConfig {
  type: BlogType;
  status: BlogStatus;
  tags: string[];
  reprintAddress?: string;
}

export interface BlogInner {
  title: string;
  content: string;
}
export interface BlogProps extends BlogConfig, BlogInner {
  prevTags?: BlogConfig["tags"];
}

/**
  博客/评论... 返回用户信息
 */
export interface IBackSenderInfo {
  _id: string;
  role: number;
  username: string;
}

type TBackSenderInfo = [IBackSenderInfo];

/**
 * @description: 服务端返回的blog信息(不含内容整体 introduce 简介)
 */
export interface Blog extends Omit<BlogProps, "content"> {
  _id: string;
  userId: string; //TODO 应该是 owner 指向博客的作者
  updateAt: number;
  createAt: number;
  browseCount: number;
  introduce: string; // 200字的介绍
  thumbsUp: number;
  thumbsDown: number;
  commentCount: number;
  sender: TBackSenderInfo;
}
/**
 * @description: 服务端返回的blog信息(含内容整体)
 */
export interface BlogDetail extends Omit<Blog, "introduce"> {
  content: string;
}

/**
 请求的范型 传入 res.data的类型
 */
export type RequestBack<T> = Promise<{ data?: T; err?: Response }>;
