import { request } from ".";
import { RequestBack, IBackSenderInfo } from "./types";
import io, { Socket } from "socket.io-client";
import { DefaultEventsMap } from "socket.io-client/build/typed-events";
import { baseURL } from "../constant/url";

export type SOCKET = Socket<DefaultEventsMap, DefaultEventsMap>;

export interface IWorldNews {
  _id: string;
  content: string;
  sender: [IBackSenderInfo];
}

interface IQueryWorldNews {
  limit?: number;
  skip?: number;
}

export const queryWorldNews = (
  params?: IQueryWorldNews
): RequestBack<IWorldNews[]> => request("/queryWorldNews", params);

interface ISendWorldNews {
  content: string;
}

export const sendWorldNews = (
  params: ISendWorldNews
): RequestBack<IWorldNews> => request("/sendWorldNews", params);

export const createConnect: () => SOCKET = () => io(baseURL);
