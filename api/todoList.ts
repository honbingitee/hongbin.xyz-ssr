import { query } from ".";

export const getTodoList = () => query("/getTodoList");
