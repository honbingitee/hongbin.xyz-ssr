import { query } from ".";
import { Blog } from "./types";

export interface IUserDetail {
  _id: string;
  username: string;
  mail?: string;
  role: number;
  blogCount: number;
  commentCount: number;
  thumbsUp: string[];
  thumbsDown: string[];
  blogs: Blog[];
}

export const getUserById = (_id: string) =>
  query("getUserById", { params: { _id } });
