import { query } from ".";
import { IBackSenderInfo } from "./types";

export interface Source {
  _id: string;
  name: string;
  description: string;
  owner: string;
  file: string;
  sender: [IBackSenderInfo];
}

interface IQuerySource {
  owner?: string;
}

export const querySource = () => query("/querySource");
