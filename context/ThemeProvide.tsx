import {
  createContext,
  useState,
  useCallback,
  ReactNode,
  useContext,
} from "react";
import { ThemeProvider } from "styled-components";
import { GlobalStyle } from "../components/BUI";
import { useMount } from "../hooks";

export const media = {
  phone: "750px",
  pc: "1400px",
};

const defaultTheme = {
  index: {
    low: -1,
    high: 1,
    higher: 3,
    fixed: 4,
    max: 999,
    mobile: 10,
    emergeMobile: 11,
  },
  contentWidth: "70rem",
  media,
  backdropFilter: `
    -webkit-backdrop-filter: blur(0.5rem);
    backdrop-filter: blur(0.5rem);
  `,
  // backdropFilter: css`
  //   -webkit-backdrop-filter: blur(0.5rem);
  //   backdrop-filter: blur(0.5rem);
  // `,
};

export const primaryColor = "#51f";

export const lightTheme = {
  primary: primaryColor,
  second: "#eee",
  fg: "#000",
  bg: "#fff",
  // nav: "transparent",
  nav: "#f7f3f35e",
  mode: "light",
  button: {
    bg: "#51f",
    color: "#fff",
  },
  text: "#000",
  shadow: "#ccc",
  lightText: "#FFF",
  thinText: "#666",
  link: "#00f",
  replaceBDFilter: "background-color:#fff !important",
};

const darkTheme = {
  primary: "#000",
  second: "#333",
  fg: "#eee",
  bg: "#000",
  nav: "#333333ad",
  mode: "dark",
  button: {
    bg: "#666",
    color: "#eee",
  },
  text: "#eee",
  shadow: "#333",
  lightText: "#ddd",
  thinText: "#ccc",
  link: "#66f",
  //当对模糊背景 backdropFilter 支持不友好的浏览器上替代方案
  replaceBDFilter: "background-color:#333 !important",
};

const ThemeContext = createContext<undefined | (() => void)>(undefined);

const fuckBrowser = ["UCBrowser", "Quark", "MQQBrowser"];
// Weixin

export const useToggleTheme = () => useContext(ThemeContext);

const ThemeProvide = ({ children }: { children: ReactNode }): JSX.Element => {
  const [theme, setTheme] = useState({ ...lightTheme, ...defaultTheme });

  const toggleMode = useCallback(() => {
    setTheme(({ mode }) => {
      const newMode = mode == "dark" ? lightTheme : darkTheme;
      localStorage.setItem("MODE", newMode.mode);
      document.body.style.backgroundColor = newMode.bg;
      return { ...newMode, ...defaultTheme };
    });
  }, []);

  useMount(() => {
    const oldMode = localStorage.getItem("MODE");
    if (oldMode && oldMode != theme.mode) {
      toggleMode();
    }
    //在uc浏览器或夸克等极为先进的浏览器对背景高斯模糊样式的支持令人发指
    //在这些浏览器中不使用背景模糊效果
    if (fuckBrowser.some(b => navigator.userAgent.indexOf(b) !== -1)) {
      Object.assign(defaultTheme, { backdropFilter: "" });
      setTheme(prev => ({ ...prev, ...defaultTheme }));
    }
  });

  return (
    <ThemeContext.Provider value={toggleMode}>
      <GlobalStyle />
      <ThemeProvider theme={theme}>{children}</ThemeProvider>
    </ThemeContext.Provider>
  );
};

export default ThemeProvide;
