const { createServer } = require('http')
const https = require("https");
const { parse } = require('url')
const express = require('express')
const path = require("path");
const fs = require("fs");
const next = require('next')

const server = express();
const port = process.env.PORT || 80;
const dev = process.env.NODE_ENV !== 'production'
const httpPort = process.env.PORT || 800;
const httpsPort = process.env.PORT || 4430;
const app = next({ dev })
const handle = app.getRequestHandler()

const buildPaths = ['/admin', "/static", '/asset-manifest.json', '/asset-manifest.json', '/logo192.png', '/logo512.png', '/manifest.json', '/robots.txt']

const privateKey = fs.readFileSync("../Nginx_SSL/2_hongbin.xyz.key", "utf8");
const certificate = fs.readFileSync(
    "../Nginx_SSL/1_hongbin.xyz_bundle.crt",
    "utf8"
);
const credentials = { key: privateKey, cert: certificate };

app.prepare().then(() => {
    https.createServer(credentials, (req, res) => {
        const parsedUrl = parse(req.url, true)
        const { pathname, query } = parsedUrl;
        console.log(pathname, query);
        const Admin = buildPaths.some(path => pathname.indexOf(path) === 0);
        if (Admin) {
            let pathName = req.url;
            console.log('hi', pathName);
            // 提供一个 icon就不会发起/favicon.ico的请求了
            if (pathName == "/admin") {
                pathName = "/index.html";
            }

            const extName = path.extname(pathName);
            fs.readFile(`../hongbin_xyz_admin${pathName}`, function (err, data) {
                if (err) {
                    console.error(err);
                    // res.status(400).json(err);
                    res.write('not found');
                } else {
                    const ext = getExt(extName);
                    res.writeHead(200, { "Content-Type": ext + "; charset=utf-8" });
                    res.write(data);
                }
                res.end();
            });
        } else {
            handle(req, res, parsedUrl)
        }
    }).listen(httpsPort, (err) => {
        if (err) throw err
        console.log('> Ready on https://hongbin.xyz:' + httpsPort)
    })
    createServer((req, res) => {
        // Be sure to pass `true` as the second argument to `url.parse`.
        // This tells it to parse the query portion of the URL.
        const parsedUrl = parse(req.url, true)
        const { pathname, query } = parsedUrl
        console.log(pathname);
        const Admin = buildPaths.some(path => pathname.indexOf(path) === 0);
        if (Admin) {
            let pathName = req.url;
            console.log('hi', pathName);
            // 提供一个 icon就不会发起/favicon.ico的请求了
            if (pathName == "/admin") {
                pathName = "/index.html";
            }

            const extName = path.extname(pathName);
            fs.readFile(`../hongbin_xyz_admin${pathName}`, function (err, data) {
                if (err) {
                    console.error(err);
                    // res.status(400).json(err);
                    res.write('not found');
                } else {
                    const ext = getExt(extName);
                    res.writeHead(200, { "Content-Type": ext + "; charset=utf-8" });
                    res.write(data);
                }
                res.end();
            });
        } else {
            handle(req, res, parsedUrl)
        }
    }).listen(httpPort, (err) => {
        if (err) throw err
        console.log('> Ready on http://hongbin.xyz:' + httpPort)
    })
})

const EXT = {
    ".html": "text/html",
    ".css": "text/css",
    ".js": "text/js",
    ".ico": "image/x-icon",
    ".png": "image/png",
};
// 获取后缀名
getExt = extName => {
    return EXT[extName];
};