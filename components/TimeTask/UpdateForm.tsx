import { FC, ReactElement, ChangeEventHandler, useRef } from "react";
import styled from "styled-components";
import { LoadButton } from "../BUI";
import { ThemeBgcAndText } from "../BUI/View/ThemeView";
import TimePick from "./TimePick";

interface IProps {
  handleSelect: ChangeEventHandler<HTMLSelectElement>;
  loading: boolean;
  onUpdate: (task: string) => void;
  handleCancelUpdate: () => void;
}

const UpdateForm: FC<IProps> = ({
  handleSelect,
  loading,
  handleCancelUpdate,
  onUpdate,
}): ReactElement => {
  const input = useRef<HTMLInputElement>(null);

  return (
    <Container>
      <div>
        <input ref={input} placeholder="提醒任务" />
        <TimePick onChange={handleSelect} />
        <div>
          <LoadButton onClick={handleCancelUpdate}>取消</LoadButton>
          <LoadButton
            onClick={() => input.current && onUpdate(input.current.value)}
            loading={loading}
          >
            确定
          </LoadButton>
        </div>
      </div>
    </Container>
  );
};

export default UpdateForm;

const Container = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: #0000004f;

  button {
    display: inline-block;
    margin: 1rem;
    min-width: 5rem;
  }

  & > div {
    display: flex;
    align-items: center;
    justify-content: space-evenly;
    flex-direction: column;
    margin: 2rem auto;
    width: 30rem;
    border-radius: 1rem;
    height: 25rem;
    ${ThemeBgcAndText};

    @media (max-width: ${props => props.theme.media.phone}) {
      width: 25rem;
    }
  }
`;
