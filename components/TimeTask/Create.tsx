import { FC, ReactElement, ChangeEventHandler } from "react";
import { Button } from "../BUI";
import LoadIcon from "../BUI/Icon/LoadIcon";
import TimePick from "./TimePick";

interface IProps {
  handleSelect: ChangeEventHandler<HTMLSelectElement>;
  onClick: () => void;
  loading: boolean;
}

const Create: FC<IProps> = ({
  handleSelect,
  onClick,
  loading,
}): ReactElement => {
  return (
    <>
      <p style={{ color: "#ccc" }}>例如:每隔1小时发邮件提醒我喝水</p>
      <TimePick onChange={handleSelect} />
      <Button style={{ zIndex: 1 }} onClick={onClick} width="10rem" margin>
        {loading ? (
          <>
            <LoadIcon />
            创建中
          </>
        ) : (
          "确定"
        )}
      </Button>
    </>
  );
};

export default Create;
