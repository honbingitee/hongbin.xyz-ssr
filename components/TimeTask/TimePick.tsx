import { FC, ReactElement, ChangeEventHandler } from "react";

interface IProps {
  onChange: ChangeEventHandler<HTMLSelectElement>;
}

const TimePick: FC<IProps> = ({ onChange }): ReactElement => {
  return (
    <div>
      当前:每隔
      <select onChange={onChange}>
        <option value="0.5">0.5</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
        <option value="9">9</option>
        <option value="10">10</option>
        <option value="11">11</option>
        <option value="12">12</option>
      </select>
      小时提醒你
    </div>
  );
};

export default TimePick;
