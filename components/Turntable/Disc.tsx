import { FC, ReactElement, useEffect, useRef } from "react";
import styled, { keyframes } from "styled-components";
import { flexCenter } from "../BUI/styled";
import { ThemePrimaryBgc } from "../BUI/View/ThemeView";

interface IProps {
  list: Array<{ color: string; content: string }>;
  select: number; //抽中哪个
}

const { PI } = Math;

const draw = (ctx: CanvasRenderingContext2D, list: IProps["list"]) => {
  const { length } = list;
  const canvasWidth = 320; //转盘长款宽一致
  const halfCanvasWidth = canvasWidth / 2; //宽度的一半 用于圆心，半径等
  ctx.clearRect(-canvasWidth, -canvasWidth, canvasWidth * 2, canvasWidth * 2);

  const radian = (2 * PI) / length; //一个扇形跨越的角度 如 一个选项占360度 两个一个占180度 三个一个120度

  const drawSector = ({ color, content }: typeof list[number]) => {
    ctx.beginPath();
    ctx.moveTo(0, 0);
    ctx.fillStyle = color;
    ctx.arc(0, 0, halfCanvasWidth, -radian / 2, radian / 2);
    ctx.fill();
    ctx.closePath();
    // 旋转画文字
    ctx.fillStyle = "#000";
    ctx.font = `${30 - length}px 微软雅黑`;
    ctx.fillText(
      `${content.length > 4 ? `${content.slice(0, 4)}...` : content}`,
      40,
      20 - length
    );
    ctx.rotate(radian);
    ctx.closePath();
  };

  for (let i = 0; i < length; i++) {
    drawSector(list[i]);
  }

  //! 后画否则会被前面的覆盖
  // 中心实心圆形
  ctx.beginPath();
  ctx.fillStyle = "#f55";

  ctx.arc(0, 0, 12, 0, 2 * PI);
  ctx.fill();

  ctx.closePath();
};

const Disc: FC<IProps> = ({ list, select }): ReactElement => {
  const canvasRef = useRef<HTMLCanvasElement>(null);
  const handRef = useRef<SVGSVGElement>(null);

  useEffect(() => {
    const canvas = canvasRef.current;
    if (!canvas) return;
    if (!canvas.getAttribute("translated")) {
      canvas.setAttribute("translated", "true");
      const ctx = canvas.getContext("2d") as CanvasRenderingContext2D;
      // ! canvas translate 只能执行一次 多次执行圆形一直偏移
      // 将 (0,0) 置于圆心
      ctx.translate(160, 160);
    }
  }, []);

  useEffect(() => {
    const canvas = canvasRef.current;
    const svg = handRef.current;
    if (!canvas || !svg) return;
    if (select !== -1) {
      canvas.style.transition = "transform 3s cubic-bezier(0.68, 1.21, 0, 1.1)";
      const { length } = list;
      const r = 360 / length;
      const transform = `rotate(${
        // 2880 + r * (length - select) - Math.random() * r - 90 - PI / length
        2880 + r * (length - select) - 90 - Math.random() * (r / 2)
      }deg)`;
      canvas.style.transform = transform;
      setTimeout(() => {
        svg.style.transform =
          "rotate(180deg) translateY(-0.7rem) translateX(0.4rem)";
        svg.style.animationPlayState = "paused";
        setTimeout(() => {
          svg.style.fill = list[select].color;
        }, 1000);
      }, 2000);
    } else {
      canvas.style.transition = "";
      canvas.style.transform = "";
      svg.style.transform = "rotate(180deg) translateX(0.4rem)";
      setTimeout(() => {
        svg.style.animationPlayState = "running";
      }, 1000);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [select]);

  useEffect(() => {
    const ctx = canvasRef.current?.getContext("2d") as CanvasRenderingContext2D;
    ctx && draw(ctx, list);
  }, [list]);

  return (
    <Wrap>
      <svg
        viewBox="0 0 1024 1024"
        version="1.1"
        xmlns="http://www.w3.org/2000/svg"
        p-id="1224"
        width="64"
        height="64"
        ref={handRef}
      >
        <path
          d="M426.666667 85.333333a85.333333 85.333333 0 0 1 85.333333 85.333334v192s85.333333-10.666667 85.333333 32c0 0 85.333333-10.666667 85.333334 32 0 0 85.333333-10.666667 85.333333 32 0 0 85.333333-10.666667 85.333333 32V640c0 42.666667-128 256-128 298.666667H384s-85.333333-298.666667-213.333333-384c0 0-42.666667-256 170.666666-42.666667V170.666667a85.333333 85.333333 0 0 1 85.333334-85.333334z"
          p-id="1225"
        ></path>
      </svg>

      <canvas width="320" height="320" ref={canvasRef}>
        浏览器必须支持canvas
      </canvas>
    </Wrap>
  );
};

export default Disc;

const move = keyframes`
0%{top: -18%};
50%{top: -20%};
100%{top: -18%};
`;

const Wrap = styled.div`
  ${flexCenter};
  position: relative;
  width: 20rem;
  height: 20rem;
  border-radius: 20rem;
  ${ThemePrimaryBgc};
  margin-bottom: 1rem;
  box-shadow: 0 0 4px 1px #666;

  & > svg {
    position: absolute;
    top: -20%;
    z-index: 1;
    transform: rotate(180deg) translateX(0.4rem);
    fill: ${props => props.theme.button.bg};
    transition: transform 1s ease-in;
    animation: ${move} 1s linear reverse infinite;
  }

  & > canvas {
    border: 1px solid ${props => props.theme.primary};
    border-radius: 50%;
    /* width: 20rem;
    height: 20rem; */
  }
`;
