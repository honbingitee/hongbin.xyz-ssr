import { IDateTime } from ".";

export const monthOperation = (prev: IDateTime, num: number) => {
  const newDate = { ...prev };
  let month = newDate.month + num;
  if (month <= 0) {
    month = 12;
    newDate.year -= 1;
  } else if (month >= 13) {
    month = 1;
    newDate.year += 1;
  }
  newDate.month = month;
  return newDate;
};

//! 移动最小1 0.x不能移动
export const animateScrollToTop = (top: number, element: HTMLElement) => () => {
  const { scrollTop, scrollHeight, clientHeight } = element;

  if (clientHeight + scrollTop < scrollHeight) {
    const diff = top - scrollTop;
    // console.log(top, scrollTop, diff);
    const dis = diff / 6;
    // const scrollDis = dis < 1 ? 1 : dis;
    const scrollDis = dis < 1 ? Math.ceil(diff / 2) : dis;

    if (scrollTop < top) {
      requestAnimationFrame(animateScrollToTop(top, element));
      element.scrollTo(0, scrollTop + scrollDis);
    }
  }
};

export const randomColor = () =>
  "#" + Math.round(Math.random() * 0xffffff).toString(16);
