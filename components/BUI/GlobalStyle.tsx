import { createGlobalStyle } from "styled-components";

const GlobalStyle = createGlobalStyle`
  html,body {
    height: 100%;
    margin:0;
    padding:0;
    font-family: "Arial Rounded MT Bold", "Helvetica Rounded", Arial, sans-serif;
    cursor: url("/plane.ico"),auto;
    font-size: 16px;
  }
  div,nav,input{
    box-sizing: border-box;
    padding: 0;
    margin: 0;
  }
 
  a {
    color: inherit;
    text-decoration: none;
  }
  a[title='站长统计']{
    display:none
  }

  * {
    box-sizing: border-box;
  }

  :root {
    --primary-color: #51f;
    --nav-height: 4rem;
    --footer-height: 20rem;
  }


  ::-webkit-scrollbar-thumb {
      background: #5511ff;
      border-radius: 4px;
  }

  ::-webkit-scrollbar {
      width: 2px;
      height: 4px;
      background-color: rgba(0, 0,0,50%);
  }


  @media screen and (max-width: 750px){
    html{
      font-size: 65%;
    }
    :root {
      --footer-height: 10rem;
    }
  }
`;

export default GlobalStyle;
