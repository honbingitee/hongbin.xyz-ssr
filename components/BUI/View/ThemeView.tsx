/*
 * @Author: hongbin
 * @Date: 2021-08-25 09:08:29
 * @LastEditTime: 2021-09-13 09:14:13
 * @Description: 有theme全局主题的styled公共组件
 * @FilePath: /hongbin_xyz_web/components/BUI/View/ThemeView.tsx
 */
import styled, { css } from "styled-components";

export const ThemeBgc = css`
  background-color: ${props => props.theme.bg};
`;

export const ThemeText = css`
  color: ${props => props.theme.text};
`;

export const ThemeBgcAndText = css`
  ${ThemeBgc};
  ${ThemeText};
`;

export const ThemePrimaryText = css`
  color: ${props => props.theme.primary};
`;

export const ThemePrimaryBgc = css`
  background-color: ${props => props.theme.primary};
`;

export const ThemeButtonBgc = css`
  background: ${props => props.theme.button.bg};
`;

export const ThemeTextDiv = styled.div`
  color: ${props => props.theme.color};
`;

export const ThemeTextBgcDiv = styled.div`
  color: ${props => props.theme.bg};
`;

export const ThemeTextPrimaryDiv = styled.div`
  color: ${props => props.theme.primary};
`;

export const ThemeDiv = styled.div`
  background-color: ${props => props.theme.bg};
`;

export const ThemePrimaryDiv = styled.div`
  background-color: ${props => props.theme.primary};
`;
