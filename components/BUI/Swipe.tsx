import { FC, memo, ReactElement, useEffect, useState } from "react";
import styled, { css, CSSObject } from "styled-components";
import { flexCenter } from "./styled";
import { ThemePrimaryBgc } from "./View/ThemeView";

interface IProps {
  cardStyle?: CSSObject;
  data: Partial<IData>[] | undefined;
}

interface IData {
  title: string;
  article: string;
  component?: ReactElement;
}

const sm = 1; //小图
const smOpacity = 0.6;
const smLateX = 20;
const ms = 2; //中图
const msOpacity = 0.8; //中图
const msLateX = 10;
const lg = 3; //大图
const lgOpacity = 1;

type Obj<T> = { [key: string]: T };

const IndexObj: Obj<number> = { sm, ms, lg };

const initial = [
  { title: "加载中..." },
  { title: "加载中..." },
  { title: "加载中..." },
  { title: "加载中..." },
  { title: "加载中..." },
];
const length = 5;
const Swipe: FC<IProps> = ({
  data = initial,
  cardStyle = {},
}): ReactElement => {
  const [index, setIndex] = useState(0);
  const [direction, setDirection] = useState(0); //左 -1 右 1

  const getDate = (index: number): Partial<IData> =>
    data![index >= length ? index - length : index];

  const handleChange = (num: 1 | -1) => () => {
    setIndex(index => {
      const next = index + num * -1;
      return next >= length ? 0 : next < 0 ? length - 1 : next;
    });
    setDirection(num * -1);
  };

  return (
    <Main>
      <ArrowIcon onClick={handleChange(-1)} />
      <Card direction={direction} data={getDate(index)} zIndex={sm} />
      <Card direction={direction} data={getDate(index + 1)} zIndex={ms} />
      <Card direction={direction} data={getDate(index + 2)} zIndex={lg} />
      <Card
        direction={direction}
        data={getDate(index + 3)}
        zIndex={ms}
        right={true}
      />
      <Card
        direction={direction}
        data={getDate(index + 4)}
        zIndex={sm}
        right={true}
      />
      <ArrowIcon onClick={handleChange(1)} right />
    </Main>
  );
};

export default memo(Swipe);

interface ICard {
  zIndex: number;
  right?: boolean;
  data: Partial<IData>;
  direction: number;
}

const Card: FC<ICard> = ({ zIndex, right, data, direction }) => {
  const [prev, setPrev] = useState(data);
  const [move, setMove] = useState(false);

  useEffect(() => {
    if (prev.component !== data.component || prev.title !== data.title) {
      setMove(true);
      setTimeout(() => {
        setPrev(data);
        setMove(false);
      }, 300);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [data]);

  return (
    <CardStyled zIndex={zIndex} right={right} move={move} direction={direction}>
      {prev.component ? (
        prev.component
      ) : (
        <>
          <h1>{prev.title}</h1>
          <article>{prev.article}</article>
        </>
      )}
    </CardStyled>
  );
};

interface ICardStyled extends Omit<ICard, "data"> {
  move: boolean;
}

const d = (
  <svg
    viewBox="0 0 1024 1024"
    version="1.1"
    xmlns="http://www.w3.org/2000/svg"
    p-id="2415"
    width="32"
    height="32"
  >
    <path
      d="M378.24 512l418.88 418.88L704 1024 192 512l512-512 93.12 93.12z"
      fill="#ffffff"
      p-id="2416"
    ></path>
  </svg>
);

const ArrowIcon = styled.div.attrs({
  children: d,
})<{ right?: boolean }>`
  padding: 3px 5px;
  background-color: #0000007d;
  position: absolute;
  ${({ right }) => (right ? "right:0;transform:rotate(-180deg)" : "left:0")};
  width: 2rem;
  height: 3rem;
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
  transition: background-color 0.3s linear, opacity 0.3s linear;
  border-radius: 3rem;
  opacity: 0;
  z-index: ${props => props.theme.index.higher};
  :hover {
    background-color: #000000c8;
  }
  @media (max-width: ${props => props.theme.media.phone}) {
    opacity: 1;
    ${({ right }) =>
      right ? "right:5px;transform:rotate(-180deg)" : "left:5px"};
    z-index: 5;
  }
`;

const CardStyled = styled.div<ICardStyled>`
  position: absolute;
  z-index: ${({ zIndex }) => zIndex};
  width: 40rem;
  height: 100%;
  ${({ zIndex, right }) =>
    zIndex !== lg &&
    css`
      transform: ${() =>
        `translateX(${
          (zIndex === sm ? smLateX : msLateX) * (right ? 1 : -1)
        }rem) scale(${zIndex === ms ? msOpacity : smOpacity})`};
      opacity: ${zIndex === sm ? smOpacity : msOpacity};
      /* filter: brightness(0.5) */
    `};
  /* ${ThemePrimaryBgc}; */
  color: #fff;
  ${flexCenter};
  flex-direction: column;
  border-radius: 0.5rem;

  article {
    width: 90%;
    margin: 0 auto;
    text-align: center;
    text-overflow: ellipsis;
    overflow: hidden;
    white-space: nowrap;
  }

  ${({ move, zIndex, right, direction }) => {
    if (move) {
      if (zIndex === lg) {
        return css`
          transition: all 0.3s linear;
          /* transition-property: opacity, transform; */
          transform: ${`translateX(${msLateX * direction * -1}rem) scale(0.8)`};
          opacity: ${msOpacity};
          z-index: ${IndexObj.ms};
        `;
      } else {
        const toMain =
          (right && direction === 1) || (!right && direction === -1);
        if (zIndex === ms) {
          const next = smLateX * (right ? 1 : -1) * direction;
          return css`
            transition: all 0.3s linear;
            transition-property: opacity, transform;
            opacity: ${toMain ? lgOpacity : smOpacity};
            transform: ${() =>
              `translateX(${toMain ? 0 : next * (right ? -1 : 1)}rem) scale(${
                toMain ? 1 : 0.6
              })`};
            ${`z-index:${IndexObj[toMain ? "lg" : "sm"]};`};
          `;
        } else {
          return css`
            transition: all 0.3s linear;
            ${toMain
              ? `transform: translate(${
                  msLateX * direction
                }rem) scale(0.8);opacity:${msOpacity}`
              : `transform: translate(${
                  msLateX * direction * 2
                }rem) scale(0.6);`};
          `;
        }
      }
    }
    return "";
  }}
  @media (max-width: ${props => props.theme.media.phone}) {
    width: 25rem;
    height: 40rem;
  }
`;

const Main = styled.div`
  width: 100%;
  height: 100%;
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;

  :hover ${ArrowIcon} {
    opacity: 1;
  }
`;
