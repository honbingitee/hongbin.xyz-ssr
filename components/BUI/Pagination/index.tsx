import { FC, memo, ReactElement } from "react";
import styled, { css } from "styled-components";
import { primaryColor } from "../../../context/ThemeProvide";
import { ActiveLateY } from "../styled";
import { ThemeBgcAndText, ThemePrimaryBgc } from "../View/ThemeView";

interface IProps {
  current: number;
  total: number;
  pageCount: number;
  onChange: (index: number) => void;
}
/**
分页组件
 * @param {number} current 当前第几页
 * @param {number} pageCount 一页多少条
 * @param {number} total 一共多少条
 * @param {func} onChange 点击换页事件 返回参数点击页数
 */
const Pagination: FC<IProps> = ({
  current,
  pageCount,
  total,
  onChange,
}): ReactElement => {
  return (
    <Container>
      {Array.from(Array(Math.ceil(total / pageCount))).map((_, index) => (
        <Box
          onClick={() =>
            current === index + 1 ? undefined : onChange(index + 1)
          }
          active={current === index + 1}
          key={index}
        >
          {index + 1}
        </Box>
      ))}
    </Container>
  );
};

export default memo(Pagination);

const Box = styled.div<{ active?: boolean }>`
  width: 3rem;
  height: 3rem;
  line-height: 3rem;
  text-align: center;
  font-weight: bold;
  margin: 0.5rem;
  cursor: pointer;
  transition: all 0.2s linear;
  border-radius: 2px;
  ${ThemeBgcAndText};

  &:hover {
    box-shadow: 0 0 0.6rem 0 ${props => props.theme.shadow};
  }
  ${ActiveLateY};
  ${props =>
    props.active &&
    css`
      background: ${primaryColor};
      color: ${props => props.theme.lightText};
    `}
`;

const Container = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`;
