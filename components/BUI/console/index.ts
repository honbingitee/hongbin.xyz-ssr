/*
 * @Author: hongbin
 * @Date: 2021-09-22 11:17:10
 * @LastEditTime: 2021-09-22 12:02:03
 * @LastEditors: Please set LastEditors
 * @FilePath: /hongbin_xyz_web/components/BUI/console/index.ts
 */

const defaultStyle =
  "font-size: 10px;padding: 2px 4px;border-radius: 3px;letter-spacing:1px;";

const warnStyle = defaultStyle + "color: #fff; background: #f88;";
const logStyle = defaultStyle + "color: #fff; background: #51f;";
const errorStyle = defaultStyle + "color: #fff; background: #f00;";

/**
    %c -  样式
    %s -  字符串
 */

export const Warn = (msg: string | Number): void => {
  console.log("%c%s", warnStyle, `警告: ${msg}`);
};

export const Log = (msg: string | Number): void => {
  console.log("%c%s", logStyle, msg);
};

export const Error = (msg: string | Number): void => {
  console.log("%c%s", errorStyle, `危险: ${msg}`);
};
