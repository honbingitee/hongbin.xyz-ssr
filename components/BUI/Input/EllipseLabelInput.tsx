import { Dispatch, FC, InputHTMLAttributes, SetStateAction } from "react";
import styled, { css } from "styled-components";
import { ThemeBgc, ThemePrimaryBgc } from "../View/ThemeView";

interface IEllipseLabelInput extends InputHTMLAttributes<HTMLInputElement> {
  type?: string;
  label: string;
  value: string;
  setValue: Dispatch<SetStateAction<string>>;
  error?: boolean;
  errorText?: string;
}

const EllipseLabelInput: FC<IEllipseLabelInput> = ({
  type = "text",
  label,
  value,
  setValue,
  error,
  errorText,
  ...props
}) => {
  return (
    <Box error={error} hasValue={!!value}>
      <input
        type={type}
        value={value}
        onChange={e => setValue(e.target.value)}
        {...props}
      />
      <span>{error ? errorText : label}</span>
    </Box>
  );
};

export default EllipseLabelInput;

const ErrorInput = css`
  border-color: #f00;
  color: #f00;
  & + span {
    top: 0;
    z-index: 1;
    background: #f00;
    color: #fff;
    font-size: 0.7rem;
    padding: 0 0.5rem;
  }
`;

const FocusInput = css`
  border-color: ${props => props.theme.primary};
  & + span {
    top: 0;
    z-index: 1;
    ${ThemePrimaryBgc};
    color: #fff;
    font-size: 0.7rem;
    padding: 0 0.5rem;
  }
`;

const Box = styled.div<{ hasValue: boolean; error?: boolean }>`
  position: relative;
  width: 100%;
  height: 3rem;
  padding-top: 0.5rem;
  margin: 0.5rem 0;

  & > input {
    height: 2.8rem;
    font-size: 1rem;
    width: inherit;
    padding: 0.5rem;
    background-color: rgba(255, 255, 255, 0.623);
    border: 1px solid #666;
    border-radius: 3px;
    position: absolute;
    padding-top: 0.75rem;
    z-index: 1;
    outline: none;

    ${props => (props.hasValue ? FocusInput : undefined)};

    :focus {
      ${props => (props.error ? ErrorInput : FocusInput)};
    }

    ${props => (props.error ? ErrorInput : undefined)};
  }

  & > span {
    position: absolute;
    left: 0.5rem;
    top: 1.25rem;
    color: #666;
    ${ThemeBgc};
    border-radius: 0.7rem;
    transition-property: color, background, top, font-size, padding;
    transition-duration: 0.3s;
  }
`;
