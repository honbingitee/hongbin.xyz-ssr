import { FC, DOMAttributes, memo } from "react";
import styled from "styled-components";
import LoadIcon from "../Icon/LoadIcon";
import { ActiveLateY } from "../styled";
import { JustifyContentProps } from "../types";

interface IButton {
  margin?: boolean;
  width?: string;
  height?: string;
  justify?: JustifyContentProps;
  column?: boolean;
  padding?: string;
  lightBgc?: string;
  darkBgc?: string;
}

const Button = styled.button<IButton>`
  width: ${props => props.width};
  background-color: ${({ theme, lightBgc, darkBgc }) => {
    if (lightBgc && darkBgc) {
      return theme.mode === "dark" ? darkBgc : lightBgc;
    }
    return theme?.button?.bg;
  }};
  color: ${props => props.theme?.button?.color};
  font-size: 0.7rem;
  border-radius: 0.25rem;
  border: none;
  text-transform: capitalize;
  height: ${props => props.height || "2rem"};
  font-weight: bold;
  ${props => props.margin && "margin:0 0.5rem"};
  display: flex;
  align-items: center;
  justify-content: ${props => props.justify || "space-evenly"};
  cursor: pointer;
  ${ActiveLateY};
  flex-direction: ${props => (props.column ? "column" : "rol")};
  padding: ${props => props.padding};
`;

interface ILoadButton extends IButton, DOMAttributes<HTMLButtonElement> {
  loading?: boolean;
}

const LoadButtonC: FC<ILoadButton> = ({ loading, children, ...props }) => (
  <Button {...props}>
    {loading ? <LoadIcon /> : null}
    {children}
  </Button>
);

export const LoadButton = memo(LoadButtonC);

export default Button;
