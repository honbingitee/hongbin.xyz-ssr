import { FC, ReactElement } from "react";
import styled, { DefaultTheme, useTheme } from "styled-components";
import { useToggleTheme } from "../../../context/ThemeProvide";
import MoonIcon from "../Icon/MoonIcon";
import SunIcon from "../Icon/SunIcon";

interface IProps {}

interface ThemeProps extends DefaultTheme {
  mode: string;
}

const MoonSunSwitch: FC<IProps> = (): ReactElement => {
  //@ts-ignore
  const { mode } = useTheme();
  const toggleTheme = useToggleTheme();

  return (
    <Container onClick={toggleTheme} mode={mode}>
      <SunIcon />
      <MoonIcon />
    </Container>
  );
};

export default MoonSunSwitch;

const Container = styled.div<{ mode?: string }>`
  position: relative;
  width: 2rem;
  height: 2rem;
  cursor: pointer;
  height: 100%;
  overflow: hidden;
  display: flex;
  align-items: center;
  justify-content: center;

  svg {
    position: absolute;
    transition-property: opacity, transform;
    transition-duration: 0.4s;
    transition-timing-function: ease-in-out;
    opacity: 0;
    transform: translateY(-100%);
    &:nth-child(odd) {
      transform: translateY(100%);
    }
  }

  svg[mode=${props => props.mode}] {
    opacity: 1;
    transform: translateY(0);
  }
`;
