import { css } from "styled-components";

const Arrow = css`
  position: relative;
  &:hover {
    &::after {
      right: 3px;
    }
    &::before {
      right: 6px;
    }
  }
  &::after {
    position: absolute;
    content: " ";
    border-radius: 2px;
    display: inline-block;
    right: 5px;
    top: 50%;
    width: 0.5rem;
    height: 0.5rem;
    border-top: 2px solid #000;
    border-right: 2px solid #000;
    transform: rotate(45deg);
    transition: right 0.3s linear;
    z-index: 1;
  }
  &::before {
    content: " ";
    position: absolute;
    height: 2px;
    width: 1rem;
    background-color: #000;
    border-radius: 1px;
    display: inline-block;
    right: 5px;
    top: 65%;
    transition: right 0.3s linear;
  }
`;

export default Arrow;
