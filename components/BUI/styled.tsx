import styled, { css } from "styled-components";

export const Box = styled.div`
  cursor: pointer;

  /* :hover {
  svg > path {
    fill: #000;
  }
} */
`;
export const ActiveLateY = css`
  &:active {
    transform: translateY(1px);
  }
`;
export const Capitalize = css`
  text-transform: capitalize;
`;

export const Uppercase = css`
  text-transform: uppercase;
`;

export const Lowercase = css`
  text-transform: lowercase;
`;

export const BetweenCenter = css`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

export const flexCenter = css`
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const SpaceStyle = css`
  ${(props: any) =>
    props.space &&
    css`
      & > * {
        margin: ${props.space};
      }
    `};
`;

export const hideScrollbar = css`
  ::-webkit-scrollbar {
    display: none;
  }
`;

export const scrollbar = css`
  ::-webkit-scrollbar-thumb {
    background: #aaa;
    border-radius: 4px;
  }
  ::-webkit-scrollbar {
    width: 8px;
    height: 8px;
    background-color: rgba(1, 1, 1, 0);
  }
`;
