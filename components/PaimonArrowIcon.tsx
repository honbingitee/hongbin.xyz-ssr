import { FC, ReactElement, memo } from "react";
import Paimon from "../assets/paimon_small.png";
import Image from "next/image";
import styled from "styled-components";

interface IProps {
  dir: "left" | "right";
  style?: any;
  left?: string;
  right?: string;
  top?: string;
  onClick: () => void;
}

const PaimonArrowIcon: FC<IProps> = ({
  dir,
  style,
  left,
  right,
  top,
  onClick,
}): ReactElement => {
  return (
    <Container
      onClick={onClick}
      dir={dir}
      style={style}
      left={left}
      right={right}
      top={top}
    >
      <div>
        <Image src={Paimon} alt="<" />
      </div>
    </Container>
  );
};

export default memo(PaimonArrowIcon);

const Container = styled.div.attrs({ "data-arrow": "" })<{
  dir: string;
  left?: string;
  right?: string;
  top?: string;
}>`
  position: absolute;
  width: 50px;
  height: 50px;
  border-radius: 25px;
  cursor: pointer;
  top: ${props => props.top};
  left: ${props => props.left};
  right: ${props => props.right};
  transition-property: opacity, box-shadow;
  transition-duration: 0.3s;
  transition-timing-function: linear;
  z-index: 1;
  background-color: ${props => props.theme.bg};
  box-shadow: 5px 5px 20px ${props => props.theme.shadow},
    -5px -5px 20px ${props => props.theme.bg};

  &:hover {
    box-shadow: 1px 9px 20px 2px ${props => props.theme.shadow},
      -1px 9px 20px 2px ${props => props.theme.bg};
    & > div img {
      transform: ${props =>
        props.dir === "left"
          ? "translate(5px, -5px) scale(1.2)"
          : "translate(-5px, -5px) rotateY(180deg) scale(1.2)"};
    }
  }

  & > div {
    position: relative;
    width: inherit;
    height: inherit;
    overflow: hidden;
    border-radius: 50%;
    img {
      transition: transform 0.3s linear;
      transform: ${props =>
        props.dir === "left"
          ? "translate(5px, -5px)"
          : "translate(-5px, -5px) rotateY(180deg)"};
    }
  }
`;

Container.defaultProps = {
  dir: "left",
};
