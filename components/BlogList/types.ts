export interface BlogIntroduce {
  _id: string;
  title: string;
  content: string;
  updateAt: string;
  cover?: string;
}
