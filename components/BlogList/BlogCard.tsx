import React, { FC, ReactElement } from "react";
import { FlexDiv } from "../BUI";
import styled from "styled-components";
import { Blog } from "../../api/types";
import { ThemeBgc, ThemeTextDiv } from "../BUI/View/ThemeView";
import { BrowseIcon, ThumbsUpIcon, CommentIcon } from "../BUI/Icon";
import { primaryColor } from "../../context/ThemeProvide";
import { useRouter } from "next/router";
import { BlogTypeTag, MarkDownText, OptionalItem } from "../commentStyled";
import NativeMarkDown from "../MD/NativeMarkDown";

interface IProps {
  blog: Blog;
}
// TODO 组件尽量和admin一致 维护一套
const BlogCard: FC<IProps> = ({ blog }): ReactElement => {
  const { push } = useRouter();

  return (
    <Container className="blogCard">
      <FlexDiv items="center" justify="center">
        <h1
          onClick={() => {
            push(`/blog/${blog._id}`);
          }}
        >
          {blog.title}
        </h1>
        {blog.sender && blog.sender.length ? (
          <>
            <h2> - </h2>
            <h2
              data-username
              onClick={() => {
                push(`/user/${blog.sender[0]._id}`);
              }}
            >
              {blog.sender[0].username}
            </h2>
          </>
        ) : null}
      </FlexDiv>

      <FlexDiv items="center" justify="space-between">
        <BlogTypeTag>{blog.type}</BlogTypeTag>
        <ThemeTextDiv>
          <span>发布时间:{new Date(blog.createAt).toLocaleString()}</span>
          <br />
          {blog.createAt !== blog.updateAt ? (
            <span>更新时间:{new Date(blog.updateAt).toLocaleString()}</span>
          ) : null}
        </ThemeTextDiv>
      </FlexDiv>
      <section>
        <NativeMarkDown content={blog.introduce} />
      </section>
      <FlexDiv items="center" justify="space-between">
        <FlexDiv>
          {blog.tags.map(tag => (
            <OptionalItem key={tag}>{tag}</OptionalItem>
          ))}
        </FlexDiv>
        <FlexDiv>
          {blog.commentCount ? (
            <OptionalItem space="2px">
              <CommentIcon />
              <span>{blog.commentCount}</span>
            </OptionalItem>
          ) : null}
          {blog.thumbsUp ? (
            <OptionalItem space="2px">
              <ThumbsUpIcon />
              <span>{blog.thumbsUp}</span>
            </OptionalItem>
          ) : null}
          {blog.browseCount ? (
            <OptionalItem space="2px">
              <BrowseIcon />
              <span>{blog.browseCount}</span>
            </OptionalItem>
          ) : null}
        </FlexDiv>
      </FlexDiv>
    </Container>
  );
};

export default BlogCard;

const maxIntroduceHeight = "30rem";

const Container = styled(FlexDiv)`
  margin: 2rem 0;
  flex-direction: column;
  padding: 0 2rem 1rem;
  color: ${props => props.theme.text};
  ${props => props.theme.backdropFilter || props.theme.replaceBDFilter};
  background-color: ${props =>
    props.theme.mode === "light" ? "#ffffff69" : "#00000069"};
  border-radius: 1rem;

  transition-property: transform, box-shadow, backdrop-filter;
  transition-timing-function: linear;
  transition-duration: 0.2s;

  &:hover {
    transform: translateY(-2px);
    box-shadow: 2px 2px 20px #666;
    ${props =>
      props.theme.backdropFilter &&
      `-webkit-backdrop-filter: blur(0.8rem);
      backdrop-filter: blur(0.8rem);`};
  }

  h1,
  h2[data-username] {
    letter-spacing: 1px;
    text-align: center;
    cursor: pointer;
    transition-property: color, letter-spacing;
    transition-timing-function: linear;
    transition-duration: 0.1s, 0.1s, 0.3s;

    &:hover {
      text-decoration: underline;
      color: ${primaryColor};
      letter-spacing: 0;
    }
  }

  & > div:first-child {
    width: 100%;
    h1 {
      margin: 0.4rem 0;
      font-size: 1.5rem;
      max-width: 100%;
      overflow: hidden;
      text-overflow: ellipsis;
      white-space: nowrap;
    }

    h2 {
      margin: 0;
      margin-left: 5px;
    }
  }

  & > section {
    max-width: 100%;
    overflow: hidden;
    margin: 1rem 0;
    padding: 1rem;
    border-radius: 2px;
    ${ThemeBgc};
    ${MarkDownText};
    position: relative;

    & > div {
      max-width: 100%;
      max-height: ${maxIntroduceHeight};
    }
    /* 只有达到最大高度后才能看到蒙版 */
    /* &::after {
      content: "";
      position: absolute;
      top: ${`calc( ${maxIntroduceHeight} - 2rem - 35px)`};
      left: 0;
      height: 100px;
      width: 100%;
      background: linear-gradient(transparent, #fff);
      -webkit-backdrop-filter: blur(3px);
      backdrop-filter: blur(3px);
    } */
  }
`;
