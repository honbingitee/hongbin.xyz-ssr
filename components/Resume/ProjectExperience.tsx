/*
 * @Author: hongbin
 * @Date: 2021-10-10 11:36:29
 * @LastEditTime: 2021-10-10 17:47:40
 * @Description: 项目经验
 * @FilePath: /hongbin_xyz_web/components/Resume/ProjectExperience.tsx
 */
import { FC, ReactElement } from "react";
import kalosgen_pc from "./images/kalosgen_pc.jpg";
import xstore1 from "./images/xstore1.jpg";
import Image from "next/image";

interface IProps {}

const ProjectExperience: FC<IProps> = (): ReactElement => {
  return (
    <div style={{ lineHeight: "1.6rem", letterSpacing: 1 }}>
      <div>
        <h3>kalosgem</h3>
        <Image src={kalosgen_pc} alt="pc" />
        <p>
          珠宝销售商店 基于shopify二次开发 使用liquid模版语法开发
          参与大多数页面开发包括主页，商品详情页，商品列表，购物车
          并增加了360度旋转拖拽查看商品功能 此网站为海外项目 预览可能需要翻墙
        </p>
        <a href="https://kalosgem.com/">kalosgem.com</a>
      </div>
      <div>
        <h3>xstore (APP & WEB)</h3>
        <div style={{ display: "flex" }}>
          <Image src={xstore1} alt="xstore" />
          <span style={{ paddingLeft: "1rem" }}>
            此APP为一个整合大量超市的平台类似美团外卖服务，是一个大型项目，商家端和客户端在一起，使用
            <a href="https://reactnative.dev/">ReactNative</a>
            的框架
            <a href="https://expo.dev/">EXPO</a>
            开发并使用采typescript 后台使用AWS的
            <a href="https://aws.amazon.com/tw/lambda/">Lambda</a>
            开发
            <br />
            包含 支付(stript) 客服/即时通讯 扫码 购物车 订单跟踪 收藏 开店
            等功能整个app均为<strong>独立一人开发</strong>
            (包括支付，购物车后端部分)
            <br />
            <strong>还有一个移动端入口网页 使用 Next.js 开发</strong>
          </span>
        </div>
      </div>
    </div>
  );
};

export default ProjectExperience;
