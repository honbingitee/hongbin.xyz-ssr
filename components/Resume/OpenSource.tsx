/*
 * @Author: hongbin
 * @Date: 2021-10-10 18:14:09
 * @LastEditTime: 2022-05-26 23:19:58
 * @Description: 开源作品
 * @FilePath: /hongbin_xyz_web/components/Resume/OpenSource.tsx
 */

import { FC, ReactElement } from "react";
import Image from "next/image";
import { getImagePath } from "../../constant/url";
import { Title } from "../../pages/resume";

interface IProps {}

const list = [
  {
    name: "图片组合",
    desc: "使用 react node MUI 使用场景 微信头像加边框 加图标",
    url: "http://hongbin.xyz:8001",
    git: "https://gitee.com/honbingitee/picture-combination",
    img: "4bd3c6c6434dd379f44a9d7459fa3126.jpeg",
  },
  {
    name: "web-db",
    desc: "在线mongodb数据库管理工具",
    url: "http://hongbin.xyz:5000",
    git: "https://gitee.com/honbingitee/web-db",
    img: "0db8a636e02f8d10be6cea72a8f8fe61.jpeg",
  },
  {
    name: "ASCII码表",
    desc: "ASCII码表",
    url: "https://hongbin.xyz/asciitable",
    git: "https://gitee.com/honbingitee/hongbin.xyz-ssr",
    img: "a18293eb8cbe672201b0113d41237eab.jpeg",
  },
  {
    name: "二维码生成器(移动端网页)",
    desc: "使用 react node 实现根据给定信息生成二维码可供下载扫描进入对应页面",
    url: "https://hongbin.xyz:8002",
    git: "https://gitee.com/honbingitee/feed-qrcode",
    img: "bcad4de126ebfc973904570f7bef9aec.jpeg",
  },
  {
    name: "井字棋",
    desc: "使用 react node socket.io 实现连机对战井字棋",
    url: "http://hongbin.xyz:8000",
    git: "https://gitee.com/honbingitee/tic-tac-toe",
    img: "16330882377490.png",
  },
];

const OpenSource: FC<IProps> = (): ReactElement => {
  return (
    <div>
      {list.map(({ name, img, desc, url, git }, index) => (
        <div key={index}>
          <Title>{name}</Title>
          <div style={{ display: "flex" }}>
            <Image
              src={`${getImagePath}${img}`}
              alt={img}
              width={400}
              height={300}
            />
            <div style={{ minWidth: 60, paddingLeft: "0.3rem" }}>
              <span>{desc}</span>
              <br />
              <a href={url}>项目地址</a>
              <br />
              <a href={git}>git地址</a>
            </div>
          </div>
        </div>
      ))}
    </div>
  );
};

export default OpenSource;
