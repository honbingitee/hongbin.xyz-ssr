/*
 * @Author: 宏斌
 * @Date: 2021-10-10 17:49:25
 * @LastEditTime: 2021-11-02 14:53:39
 * @Description: 技术栈介绍
 * @FilePath: /hongbin_xyz_web/components/Resume/TechnologyStack.tsx
 */
import { FC, ReactElement } from "react";

interface IProps {}

const stack = {
  HTML: 5,
  CSS: 5,
  JavaScript: 5,
  React: 5,
  ReactNative: 2,
  Node: 3,
  express: 3,
  EXPO: 3,
  redux: 2,
  Vue: 2,
  VueX: 2,
  "uni-app": 2,
  Taro: 3,
  "Next.js": 4,
  "styled-component": 5,
  less: 2,
  sass: 2,
  Antd: 2,
  Material: 2,
  GraphQL: 2,
};

const TechnologyStack: FC<IProps> = (): ReactElement => {
  return (
    <div style={{ flex: 1, display: "flex", flexWrap: "wrap" }}>
      {Object.entries(stack).map(([key, level]) => (
        <strong
          key={key}
          style={{
            padding: "1rem",
            fontSize: `1.${level}rem`,
          }}
        >
          {key}
        </strong>
      ))}
    </div>
  );
};

export default TechnologyStack;
