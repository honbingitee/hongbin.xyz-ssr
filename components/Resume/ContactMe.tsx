/*
 * @Author: hongbin
 * @Date: 2021-10-10 08:05:20
 * @LastEditTime: 2021-10-10 11:00:06
 * @Description: 联系方式
 * @FilePath: /hongbin_xyz_web/components/Resume/ContactMe.tsx
 */

import { FC, ReactElement } from "react";
import wechat from "../../assets/wechat1.jpg";
import csdn from "../../assets/csdn.jpg";
import qq from "../../assets/qq.jpg";
import gongzhonghao from "../../assets/gongzhonghao .jpg";
import Image from "next/image";

interface IProps {}

const ContactMe: FC<IProps> = (): ReactElement => {
  return (
    <>
      <Image src={wechat} alt="hi17614251298" width={300} height={400} />
      <Image src={csdn} alt="printf_hello" width={300} height={400} />
      <Image src={qq} alt="2218176087" width={300} height={300} />
      <Image src={gongzhonghao} alt="前端开发手册" height={100} width={300} />
    </>
  );
};

export default ContactMe;
