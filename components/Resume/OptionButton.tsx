/*
 * @Author: hongbin
 * @Date: 2021-10-09 06:37:56
 * @LastEditTime: 2021-10-09 20:19:01
 * @LastEditors: Please set LastEditors
 * @Description: 上/下一张
 * @FilePath: /hongbin_xyz_web/components/Resume/OptionButton.tsx
 */
import { FC, memo, ReactElement } from "react";
import styled from "styled-components";
import { UpIcon, DownIcon } from "../BUI/Icon";
import { ActiveLateY } from "../BUI/styled";

interface IProps {
  onClickUp: () => void;
  onClickDown: () => void;
}

const OptionButton: FC<IProps> = ({ onClickUp, onClickDown }): ReactElement => {
  return (
    <Wrapper>
      <UpIcon onClick={onClickUp} />
      <DownIcon onClick={onClickDown} />
    </Wrapper>
  );
};

export default memo(OptionButton);

const Wrapper = styled.div`
  svg {
    box-shadow: ${props =>
      props.theme.mode === "dark"
        ? "9px 9px 18px #3a3232, -9px -9px 18px #302f2f"
        : "9px 9px 18px #c3c3c3, -9px -9px 18px #ffffff"};
    border-radius: 3px;
    cursor: pointer;
    transition: all 0.3s linear;
    ${ActiveLateY};
    fill: ${props => props.theme.text};
  }

  position: absolute;
  bottom: 0.5rem;
  right: 1rem;
  width: 5rem;
  display: flex;
  justify-content: space-between;
`;
