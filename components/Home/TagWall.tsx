import { useRouter } from "next/router";
import { FC, ReactElement, useState } from "react";
import styled from "styled-components";
import { getAllBlogTags, ITag } from "../../api/tagApi";
import { useMount } from "../../hooks";
import { flexCenter } from "../BUI/styled";
import { greyBorder } from "./Quality";

interface IProps {}

const TagWall: FC<IProps> = (): ReactElement => {
  const [tags, setTags] = useState<ITag[]>([]);
  const { push } = useRouter();

  useMount(() => {
    getAllBlogTags().then(({ data }) => {
      setTags(data);
    });
  });

  return (
    <Container>
      {tags.map(({ name, count, _id }) => (
        <div
          onClick={() => {
            push("blogList?tag=" + name);
          }}
          style={{ minWidth: `${count > 10 ? 10 : count}rem` }}
          key={_id}
        >
          {name} ({count})
        </div>
      ))}
    </Container>
  );
};

export default TagWall;

const Container = styled.div`
  display: flex;
  flex-wrap: wrap;
  padding: 1rem;
  margin: 4rem 0;
  overflow-y: hidden;

  & > div {
    margin: 5px;
    border-radius: 0.2rem;
    ${greyBorder};
    ${props => props.theme.backdropFilter || props.theme.replaceBDFilter};
    padding: 5px;
    ${flexCenter};
    cursor: pointer;
    transition: all 0.3s 0.1s linear;
    color: #444;
    :hover {
      background-color: #666;
      color: #fffae5;
      border-color: #fffae5;
    }
  }
`;
