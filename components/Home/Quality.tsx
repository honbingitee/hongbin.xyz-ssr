/*
 * @Author: hongbin
 * @Date: 2021-11-03 08:25:33
 * @LastEditTime: 2021-11-22 19:25:53
 * @Description: 前端开发的优秀品质
 * @FilePath: /hongbin_xyz_web/components/Home/Quality.tsx
 */
import { FC, ReactElement, useEffect } from "react";
import styled, { css, keyframes } from "styled-components";
import { randomColor } from "../BUI/DateTimePicker/utils";
// import "../../constant/VanillaTilt";
import addDrag from "../../constant/3DDrag";

interface IProps {}

const initialData: { title: string; content: string; desc?: string }[] = [
  { title: "极客精神", content: "好奇之心", desc: "改变之力" },
  { title: "开源精神", content: "开创", desc: "共享" },
  { title: "终身学习", content: "学无止境", desc: "不断探索" },
  { title: "团队精神", content: "相信自己", desc: "相信伙伴" },
];

const Quality: FC<IProps> = (): ReactElement => {
  useEffect(() => {
    addDrag(".qualityCard", ".child");
  }, []);

  return (
    <Wrap>
      {initialData.map((item, index) => (
        <CardStyled
          className="qualityCard"
          style={{
            //   @ts-ignore
            "--after-bgc": `${randomColor()}aa`,
            //   @ts-ignore
            "--before-bgc": `${randomColor()}aa`,
            //   @ts-ignore
            "--before-duration": `${5 + Math.floor(Math.random() * 10)}s`,
            //   @ts-ignore
            "--after-duration": `${5 + Math.floor(Math.random() * 10)}s`,
          }}
          key={item.title}
        >
          <Index className="child">0{index + 1}</Index>
          <Title className="child">{item.title}</Title>
          <Content>
            <code className="child">{item.content}</code>
            {item.desc ? (
              <>
                <br />
                <code className="child">{item.desc}</code>
              </>
            ) : null}
          </Content>
        </CardStyled>
      ))}
    </Wrap>
  );
};

export default Quality;

const Content = styled.article`
  color: #fffae5;
  font-size: 1.5rem;
  text-align: center;
  font-weight: bold;
  transition: opacity 0.2s linear, transform 0.3s linear,
    text-shadow 0.3s linear;
  opacity: 0;
  transform: translateY(5rem);
  height: 10rem;
  background-color: #01010126;
  padding-top: 1rem;
`;

const Title = styled.p`
  font-size: 2rem;
  color: #777;
  text-shadow: 1px 1px #ccc;
  letter-spacing: 7px;
  text-align: right;
  font-weight: bolder;
  margin-top: 6rem;
  transition: all 0.3s linear;
`;

const Index = styled.strong`
  font-size: 5rem;
  color: #777;
  position: absolute;
  right: 0;
  top: 0;
  text-shadow: 2px 2px #ccc;
  letter-spacing: 7px;
  transition: all 0.3s linear;
`;

const downAnimate = keyframes`
0%{transform: translate(0px,0px) };
30%{transform: translate(0.9rem,0.9rem) };
60%{transform: translate(-0.9rem,-0.9rem) };
100%{transform: translate(0px,0px) };
`;

export const greyBorder = css`
  border-top: #999 2px solid;
  border-left: #aaa 2px solid;
  border-right: #999 2px solid;
  border-bottom: #aaa 2px solid;
`;

const CardStyled = styled.div`
  width: 15rem;
  height: 18rem;
  border-radius: 1rem;
  margin: 1rem 0;
  position: relative;
  font-family: fantasy;
  transition: background 0.3s linear;
  background: #96969644;
  ${props => props.theme.backdropFilter || props.theme.replaceBDFilter};
  ${greyBorder};
  overflow: hidden;

  ::before {
    content: "";
    position: absolute;
    width: 6rem;
    height: 6rem;
    z-index: -1;
    border-radius: 6rem;
    background-color: var(--before-bgc);
    top: -2rem;
    left: -2rem;
    animation: ${downAnimate} var(--before-duration) linear infinite;
  }
  ::after {
    content: "";
    position: absolute;
    width: 10rem;
    height: 10rem;
    z-index: -1;
    border-radius: 10rem;
    background-color: var(--after-bgc);
    right: -2rem;
    bottom: -2rem;
    animation: ${downAnimate} var(--after-duration) linear infinite;
  }

  :hover {
    background: #18171794;
    ${Content} {
      opacity: 1;
      transform: translateY(0);
      text-shadow: 1px 1px #ccc;
    }
    ${Index},${Title} {
      transform: translateY(-1rem);
      color: #d3d0c3;
    }
  }
  @media (max-width: ${props => props.theme.media.phone}) {
    width: 48%;
    margin: 1% 0;
  }
`;

const Wrap = styled.div`
  width: 100%;
  display: flex;
  display: flex;
  justify-content: space-evenly;
  flex-wrap: wrap;
  overflow: hidden;
  /**
 *! overflow: hidden;  至关重要  在safari中 父元素不添加overflow 子元素 rotate会穿插别的元素 页面简直没法看
 */
`;

// interface ICard {}

// const Card: FC<ICard> = () => {
//   return <CardStyled> </CardStyled>;
// };
