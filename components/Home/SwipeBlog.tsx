import { FC, ReactElement, useEffect, useState } from "react";
import styled from "styled-components";
import { getBlogs } from "../../api/blogApi";
import { BlogStatus } from "../../api/types";
import BlogCard from "../../components/BlogList/BlogCard";
import Swipe from "../BUI/Swipe";
import { ThemeBgc } from "../BUI/View/ThemeView";

interface IProps {}

const SwipeBlog: FC<IProps> = (): ReactElement => {
  const [blogList, setBlogList] = useState<
    Array<{ component: ReactElement }> | undefined
  >(undefined);

  useEffect(() => {
    (async () => {
      const { data } = await getBlogs({
        find: { status: BlogStatus["PUBLIC"] },
        sort: { createAt: -1 },
        verify: false,
        limit: 5,
      });
      if (!data) return;
      const { blogs } = data;

      blogs &&
        setBlogList(
          blogs.map(blog => ({
            component: <BlogCard blog={blog} key={blog._id} />,
          }))
        );
    })();
  }, []);

  return (
    <Wrap>
      <Swipe data={blogList} />
    </Wrap>
  );
};

export default SwipeBlog;

const Wrap = styled.div`
  width: 100%;
  max-width: 100vw;
  overflow: hidden;
  height: 24rem;
  margin: 7rem auto;

  [direction] {
    /* background: ${props => props.theme.bg}; */
    /* border-radius: 2rem; */
    /* & > div {
      padding-bottom: 0 !important;
    } */
  }

  .blogCard {
    width: 100%;
    height: 21rem;
    -webkit-backdrop-filter: none !important;
    backdrop-filter: none !important;
    margin: 0;
    padding-bottom: 0.5rem;
    ${ThemeBgc};
    h1 {
      max-width: 60% !important;
    }

    section {
      margin: 0.5rem 0 !important;
      flex: 1;
      .markdown-body {
        max-height: 10rem;
      }
    }
  }
  @media (max-width: ${props => props.theme.media.phone}) {
    height: 48rem;
    margin: 2rem auto;

    .blogCard {
      height: 40rem;

      .markdown-body {
        max-height: 28rem !important;
      }
    }
  }
`;
