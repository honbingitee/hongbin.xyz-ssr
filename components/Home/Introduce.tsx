import { FC, ReactElement, useRef, useState } from "react";
import styled, { css } from "styled-components";
import { Button } from "../BUI";
import { flexCenter } from "../BUI/styled";
import { ThemeBgc, ThemePrimaryBgc, ThemeText } from "../BUI/View/ThemeView";

interface IProps {}

const show = { "z-index": 1, opacity: 1 };
const hide = { "z-index": -1, opacity: 0 };

const turn = [
  {
    "transition-duration": "0.2s",
    transform: "rotateY(18deg) skew(0deg, 15deg)",
  },
  {
    "transition-duration": "0.6s",
    transform: "rotateY(162deg) skew(0deg, 10deg)",
  },
  {
    "transition-duration": "0.2s",
    transform: "rotateY(180deg)",
  },
  {
    "border-color": "#000",
  },
];

const closeStyle = [
  {
    "transition-duration": "0.2s",
    transform: "rotateY(162deg) skew(0deg, 10deg)",
  },
  {
    "transition-duration": "0.6s",
    transform: "rotateY(18deg) skew(0deg, 15deg)",
  },
  {
    "transition-duration": "0.2s",
    transform: "",
  },
  { "transition-duration": "0.2s ,0.2s", "border-color": "transparent" },
];

const Introduce: FC<IProps> = (): ReactElement => {
  const coverRef = useRef<HTMLDivElement>(null);
  const [turnState, setTurn] = useState(false);

  const open = () => {
    setTurn(true);
    const cover = coverRef.current;
    if (cover) {
      Object.assign(cover.style, turn[0]);

      setTimeout(() => {
        Object.assign(cover.style, turn[1]);
        setTimeout(() => {
          Object.assign(cover.style, turn[2]);
        }, 600);
      }, 200);
      setTimeout(() => {
        // @ts-ignore
        Object.assign(cover.querySelector("[data-close]").style, show);
        // @ts-ignore
        Object.assign(cover.querySelector("[data-open]").style, hide);
        Object.assign(cover.style, turn[3]);
      }, 500);
    }
  };

  const close = () => {
    setTurn(false);
    const cover = coverRef.current;
    if (cover) {
      Object.assign(cover.style, closeStyle[0]);

      setTimeout(() => {
        Object.assign(cover.style, closeStyle[1]);
        setTimeout(() => {
          Object.assign(cover.style, closeStyle[2]);
        }, 600);
      }, 200);
      setTimeout(() => {
        // @ts-ignore
        Object.assign(cover.querySelector("[data-open]").style, show);
        // @ts-ignore
        Object.assign(cover.querySelector("[data-close]").style, hide);
        Object.assign(cover.style, closeStyle[3]);
      }, 500);
    }
  };

  return (
    <Container>
      <Aside turn={turnState}>
        <Cover ref={coverRef}>
          <div>
            <div data-open onClick={open} />
            <div data-close onClick={close} />
          </div>
        </Cover>
        <Page>
          <h2>你好我是 宏斌 👋</h2>
          <p>我是一名前端开发工程师🦁️</p>
          <section>
            我喜欢 <Part>React</Part>
            <Part>原神</Part>
            <Part>JavaScript</Part>
            <Part>肌肉</Part>
            <Part>精美的UI</Part>
            <Part>优雅的代码</Part>
            <Part>炸鸡汉堡啤酒</Part>
            <Part>编程能力臻至化境</Part>
          </section>
          <Button as="a" href="/resume">
            更多关于在下
          </Button>
        </Page>
      </Aside>
      <Aside></Aside>
    </Container>
  );
};

export default Introduce;

const Part = styled.span`
  ${ThemePrimaryBgc};
  border-radius: 0.2rem;
  height: 1.1rem;
  padding: 0 6px;
  font-size: 0.7rem;
  margin: 0.1rem 0.2rem;
  color: ${props => props.theme.lightText};
  ${flexCenter};
  letter-spacing: 1px;
`;

const style = css`
  width: 50%;
  height: 100%;
  ${ThemeBgc};
  right: 0;
  top: 0;
  position: absolute;
  border-top-right-radius: 0.5rem;
  border-bottom-right-radius: 0.5rem;
  border-top: 1px solid #ccc !important;
  border-right: 1px solid #ccc !important;
  box-shadow: 1px 1px 5px ${props => props.theme.shadow};
`;

const Page = styled.div`
  ${style};
  padding: 1rem;
  ${ThemeText};

  & > section {
    display: flex;
    flex-wrap: wrap;
    align-items: center;
  }
  & > a {
    width: 100%;
    margin-top: 1rem !important;
  }
`;

const Cover = styled.div`
  ${style};
  transition-property: transform, border-color;
  transition-timing-function: linear;
  transform-origin: left;
  z-index: 2;
  border-left: 1px solid transparent;
  display: flex;
  & > div {
    display: flex;
    flex: 1;
    position: relative;
    & > div {
      position: absolute;
      width: 100%;
      height: 100%;
      cursor: pointer;
      background-size: 90% 100% !important;
    }
    [data-close] {
      opacity: 0;
      z-index: -1;
      background: url("/bye.svg") no-repeat;
    }
    [data-open] {
      background: url("/hello.svg") no-repeat;
    }
  }
`;

const Aside = styled.aside<{ turn?: boolean }>`
  flex: 1;
  position: relative;
  transition: transform 1s linear;
  transform: ${props => `translateX(${props.turn ? "0" : "-25%"})`};
`;

const Container = styled.div`
  width: 100%;
  height: 20rem;
  margin-bottom: 5rem;
  display: flex;

  @media (max-width: ${props => props.theme.media.phone}) {
    height: 40rem;
    flex-direction: column;
    & > aside {
      width: 96%;
      margin-left: 2%;
      h2 {
        margin: 0;
      }
    }
  }
`;
