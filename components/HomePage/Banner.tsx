import { FC, ReactElement, useEffect, useRef } from "react";
import styled from "styled-components";
import { flexCenter } from "../BUI/styled";
import { AbsoluteFillView } from "../BUI";
import { ThemeBgc } from "../BUI/View/ThemeView";
import { gridLine, technologyLine, Heart } from "../../utils/canvasUtils";

interface IProps {}

const Banner: FC<IProps> = (): ReactElement => {
  const canvasRef = useRef<HTMLCanvasElement>(null);
  const backCanvasRef = useRef<HTMLCanvasElement>(null);

  useEffect(() => {
    const canvas = canvasRef.current;
    const backCanvas = backCanvasRef.current;

    const { offsetWidth, offsetHeight } = document.body;
    if (canvas) {
      const { random } = Math;

      const ctx = canvas.getContext("2d") as CanvasRenderingContext2D;

      canvas.width = offsetWidth;
      canvas.height = offsetHeight;

      const heartLength = 20;
      const heartList: any = [];
      const size = offsetWidth < 750 ? 1 : 3;
      for (let i = 0; i < heartLength; i++) {
        const x = random() * (offsetWidth - 200) + 100;
        const y = random() * offsetHeight + offsetHeight;
        const r = random() * 4 + size;
        const speed = r;
        // @ts-ignore
        heartList.push(new Heart(ctx, x, y, r, speed));
      }

      const moveUp = () => {
        ctx.clearRect(0, 0, offsetWidth, offsetHeight);
        let overItemLength = 0; //已经在屏幕外边的❤️

        for (const heart of heartList) {
          //! 直接删除超出屏幕的元素会导致页面闪灼
          if (heart.y > -heart.r * 20) {
            heart.moveUp();
          } else overItemLength++;
        }

        overItemLength != heartList.length && setTimeout(moveUp, 16);
        // if (overItemLength == heartList.length) {
        //   console.log("over");
        // }
      };

      moveUp();
    }

    if (!backCanvas) return;

    const backCtx = backCanvas.getContext("2d") as CanvasRenderingContext2D;

    //绘制 背景
    backCanvas.width = offsetWidth;
    backCanvas.height = offsetHeight;

    gridLine(
      backCtx,
      offsetWidth,
      offsetHeight,
      offsetWidth < 750 ? offsetWidth / 3 : offsetWidth / 6,
      offsetWidth < 750 ? 150 : 300
    );
    technologyLine(backCtx, offsetHeight, offsetWidth, 50);
  }, []);

  return (
    <Container>
      <Background />
      <canvas ref={canvasRef}></canvas>
      {/* <canvas ref={backCanvasRef}></canvas> */}
    </Container>
  );
};

export default Banner;

const Background = styled(AbsoluteFillView)`
  position: fixed;
  z-index: -10;
  ${props => props.theme.mode === "dark" && "filter: brightness(0.5);"};
  @media (max-width: ${props => props.theme.media.phone}) {
    background: url("/youla_zip.jpg") 0% 0% / cover no-repeat;
  }
  @media (min-width: ${props => props.theme.media.phone}) {
    /* background: url("/wallhaven-o352pm.jpg") 0% 0% / cover no-repeat; */
    /* background: url("/pc_back.avif") 0% 0% / cover no-repeat; */
    background: url("/youla.webp") 0% 0% / cover no-repeat;
    /* background: url("/youla1.webp") 0% 0% / cover no-repeat; */
  }
`;

const Container = styled.div`
  ${ThemeBgc};
  width: 100vw;
  position: relative;
  ${flexCenter};

  canvas {
    margin: 0;
    position: fixed;
    top: 0;
    left: 0;
    z-index: -1;

    :last-child {
      /* z-index: -1; */
    }
  }
`;
