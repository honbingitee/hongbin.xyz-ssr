import { FC, ReactElement, useCallback, useEffect, useState } from "react";
import Image from "next/image";
import Paimon from "../../assets/paimon_small.webp";
import styled from "styled-components";

interface IProps {}

const ScrollToTop: FC<IProps> = (): ReactElement => {
  const [state, setState] = useState(0);
  const [warning, setWarning] = useState(0);

  const listenScroll = useCallback(() => {
    if (window?.scrollY > 100 && window?.scrollY < 400) {
      state !== 1 && setState(1);
    } else if (window?.scrollY > 400) {
      state !== 2 && setState(2);
    } else {
      state && setState(0);
    }
  }, [state]);

  useEffect(() => {
    window.addEventListener("scroll", listenScroll);
    return () => {
      window.removeEventListener("scroll", listenScroll);
    };
  }, [listenScroll, state]);

  const scrollToTop = () => {
    const sTop = document.documentElement.scrollTop || document.body.scrollTop;
    if (sTop > 0) {
      window.requestAnimationFrame(scrollToTop);
      window.scrollTo(0, sTop - sTop / 8);
    }
  };

  const handleMouseEnter = () => {
    setWarning(1);
  };
  const handleMouseLeave = () => {
    setWarning(0);
  };

  return (
    <Container show={state} warning={warning} onClick={scrollToTop}>
      <Image width="100" height="200" src={Paimon} alt="paimon" />
      <section>你礼貌吗👮‍♀️</section>
      <span onMouseEnter={handleMouseEnter} onMouseLeave={handleMouseLeave}>
        👮‍♀️
      </span>
    </Container>
  );
};

export default ScrollToTop;

const Container = styled.div<{ show?: number; warning: number }>`
  position: fixed;
  bottom: 100px;
  transition-property: opacity, right, transform;
  transition-duration: 0.5s;
  transform-origin: center;
  transition-timing-function: ease-out;
  right: ${({ show }) => (show ? (show == 1 ? "-70px" : "0") : "-100px")};
  ${({ show }) => (show == 1 ? "transform: rotate(-40deg);" : undefined)}
  opacity: ${props => (props.show ? 1 : 0)};
  z-index: ${props => props.theme.index.fixed};
  cursor: pointer;

  &:hover {
    right: 0;
    transform: rotate(0);
  }
  section {
    content: "点哪呢👮‍♀️";
    white-space: nowrap;
    font-size: 12px;
    padding: 2px 6px;
    position: absolute;
    border-radius: 2px;
    top: 30%;
    right: 100%;
    background-color: ${props => props.theme.second};
    color: ${props => props.theme.text};
    z-index: ${props => props.theme.index.fixed};
    transition: opacity 0.4s ease-in;
    opacity: ${props => props.warning};

    ::before {
      content: " ";
      width: 6px;
      height: 6px;
      position: absolute;
      left: calc(100% - 3px);
      top: 5px;
      background-color: ${props => props.theme.second};
      transform: rotate(45deg);
    }
  }
  span {
    white-space: nowrap;
    font-size: 12px;
    position: absolute;
    top: 60%;
    left: 70%;
    width: 50%;
    transform: translate(-50%);
    z-index: ${props => props.theme.index.fixed};
    /* background-color: #fff; */
    /* opacity: 0; */
  }

  @media screen and (max-width: ${props => props.theme.media.phone}) {
    width: 50px;
    height: 100px;
    ${props => props.theme.mode === "dark" && "filter: brightness(0.5);"};
  }
`;
