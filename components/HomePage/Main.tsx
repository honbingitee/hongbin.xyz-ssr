import { FC, ReactElement } from "react";
import styled from "styled-components";
import { FlexDiv, WaveText } from "../BUI";
import Recommend from "./Recommend";
import KnowMe from "./KnowMe";
import BlogList from "../BlogList";
import { hideInPhone } from "../commentStyled";

interface IProps {}

const Main: FC<IProps> = (): ReactElement => {
  return (
    <Container>
      <WaveText />
      <FlexDiv
        data-hide-in-phone
        items="center"
        justify="space-between"
        style={{ marginTop: "20rem", marginBottom: "5rem" }}
      >
        <Recommend />
        <KnowMe />
      </FlexDiv>
      <BlogList />
    </Container>
  );
};

export default Main;

const Container = styled.main`
  width: ${props => props.theme.contentWidth};
  min-height: 100vh;
  max-width: 100vw;
  margin: 20rem auto 0;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: flex-start;

  & > div[data-hide-in-phone] {
    ${hideInPhone};
  }
`;
