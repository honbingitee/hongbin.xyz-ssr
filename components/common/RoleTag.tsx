import { memo, FC, ReactElement } from "react";
import styled from "styled-components";
import { primaryColor } from "../../context/ThemeProvide";

interface IProps {
  role: number;
}

const roles = ["游客", "管理者", "站长"];
const rolesColor = ["#bbb", "#faa", primaryColor];

/**
 根据用户角色返回标签
 * @param {number} role
 */
const RoleTag: FC<IProps> = ({ role }): ReactElement => {
  return <Tag Role={role}>{roles[role]}</Tag>;
};

export default memo(RoleTag);

const Tag = styled.span<{ Role: number }>`
  padding: 0.1rem 0.4rem;
  border-radius: 3px;
  background: ${props => rolesColor[props.Role]};
  color: #fff;
  font-size: 0.7rem;
  margin-left: 0.3rem;
  user-select: none;
`;
