/* eslint-disable react/display-name */
import {
  ReactElement,
  useRef,
  useMemo,
  forwardRef,
  useEffect,
  useImperativeHandle,
  useCallback,
} from "react";
import styled from "styled-components";
import ReactMarkdown from "react-markdown";
import { Prism as SyntaxHighlighter } from "react-syntax-highlighter";
import style from "../../constant/tomorrow";
import gfm from "remark-gfm";
import raw from "rehype-raw";
import "github-markdown-css";
import Toc from "./Toc";
import CopyButton from "./CopyButton";

interface IProps {
  content: string;
  syncScroll: boolean;
  setEditScroll?: (y: number) => void;
}

const MarkDown = forwardRef<(y: number) => void, IProps>(
  ({ content, syncScroll, setEditScroll }, ref): ReactElement => {
    const wrap = useRef<HTMLDivElement | null>(null);

    const setScrollTop = useCallback((y: number) => {
      //@ts-ignore
      wrap.current.scrollTop = y;
    }, []);

    useImperativeHandle(ref, () => setScrollTop, [setScrollTop]);

    const onScroll = useCallback(
      e => {
        setEditScroll!(e.target.scrollTop);
      },
      [setEditScroll]
    );

    useEffect(() => {
      if (syncScroll) {
        wrap.current?.addEventListener("scroll", onScroll);
      } else {
        wrap.current?.removeEventListener("scroll", onScroll);
      }
      return () => {
        // eslint-disable-next-line react-hooks/exhaustive-deps
        wrap.current?.removeEventListener("scroll", onScroll);
      };
    }, [onScroll, syncScroll]);

    const components = useMemo(
      () => ({
        code({ node, inline, className, children, ...props }: any) {
          const match = /language-(\w+)/.exec(className || "");
          return !inline && match ? (
            <SyntaxHighlighter
              style={style}
              language={match[1]}
              PreTag="div"
              {...props}
            >
              <>
                <CopyButton />
                {String(children).replace(/\n$/, "")}
              </>
            </SyntaxHighlighter>
          ) : (
            <code className={className} {...props}>
              {children}
            </code>
          );
        },
        h1: ({ children }: any) => (
          <h1 className="data-toc" data-toc="1">
            {children}
          </h1>
        ),
        h2: ({ children }: any) => (
          <h2 className="data-toc" data-toc="2">
            {children}
          </h2>
        ),
        h3: ({ children }: any) => (
          <h3 className="data-toc" data-toc="3">
            {children}
          </h3>
        ),
        h4: ({ children }: any) => (
          <h4 className="data-toc" data-toc="4">
            {children}
          </h4>
        ),
        h5: ({ children }: any) => (
          <h5 className="data-toc" data-toc="5">
            {children}
          </h5>
        ),
        h6: ({ children }: any) => (
          <h6 className="data-toc" data-toc="6">
            {children}
          </h6>
        ),
        p: ({ children }: any) =>
          String(children) === "@[toc]" ? (
            <Toc
              HElements={
                wrap.current?.getElementsByClassName("data-toc") as any
              }
            />
          ) : (
            <p>{children}</p>
          ),
      }),
      []
    );

    return (
      <MDWrap ref={wrap}>
        <ReactMarkdown
          //@ts-ignore
          rehypePlugins={[raw]}
          //@ts-ignore
          remarkPlugins={[gfm]}
          components={components}
        >
          {content}
        </ReactMarkdown>
      </MDWrap>
    );
  }
);

MarkDown.displayName = "MarkDownName";

export default MarkDown;

export const MDWrap = styled.div.attrs({ className: "markdown-body" })`
  flex: 1;
  width: 100%;
`;
