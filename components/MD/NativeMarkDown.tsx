/* eslint-disable react/display-name */
import { FC, ReactElement } from "react";
import { MDWrap } from "./MarkDown";
import ReactMarkdown from "react-markdown";
import { Prism as SyntaxHighlighter } from "react-syntax-highlighter";
import style from "../../constant/tomorrow";
import gfm from "remark-gfm";
import raw from "rehype-raw";
import "github-markdown-css";

interface IProps {
  content: string;
}

const component = {
  code({ node, inline, className, children, ...props }: any) {
    const match = /language-(\w+)/.exec(className || "");
    return !inline && match ? (
      <SyntaxHighlighter
        style={style}
        language={match[1]}
        PreTag="div"
        {...props}
      >
        {String(children).replace(/\n$/, "")}
      </SyntaxHighlighter>
    ) : (
      <code className={className} {...props}>
        {children}
      </code>
    );
  },
  h1: ({ children }: any) => (
    <h1 className="data-toc" data-toc="1">
      {children}
    </h1>
  ),
  h2: ({ children }: any) => (
    <h2 className="data-toc" data-toc="2">
      {children}
    </h2>
  ),
  h3: ({ children }: any) => (
    <h3 className="data-toc" data-toc="3">
      {children}
    </h3>
  ),
  h4: ({ children }: any) => (
    <h4 className="data-toc" data-toc="4">
      {children}
    </h4>
  ),
  h5: ({ children }: any) => (
    <h5 className="data-toc" data-toc="5">
      {children}
    </h5>
  ),
  h6: ({ children }: any) => (
    <h6 className="data-toc" data-toc="6">
      {children}
    </h6>
  ),
};

/**
 只有最基础的展示markdown文档功能
 */
const NativeMarkDown: FC<IProps> = ({ content }): ReactElement => {
  return (
    <MDWrap>
      <ReactMarkdown
        //@ts-ignore
        rehypePlugins={[raw]}
        //@ts-ignore
        remarkPlugins={[gfm]}
        components={component}
      >
        {content}
      </ReactMarkdown>
    </MDWrap>
  );
};

export default NativeMarkDown;
