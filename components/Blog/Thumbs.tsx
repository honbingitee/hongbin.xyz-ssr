import { FC, ReactElement, memo, useState } from "react";
import { OptionalItem } from "../commentStyled";
import { ThumbsUpIcon } from "../BUI/Icon";
import { ThumbsState } from "../../pages/blog/[id]";
import LoadIcon from "../BUI/Icon/LoadIcon";

interface IProps {
  handleThumbs: (type: ThumbsState) => () => void;
  thumbsUp: number;
  thumbsDown: number;
  thumbsState: ThumbsState;
  thumbsLoading: ThumbsState;
}

const Thumbs: FC<IProps> = ({
  handleThumbs,
  thumbsUp,
  thumbsDown,
  thumbsState,
  thumbsLoading,
}): ReactElement => {
  return (
    <>
      <OptionalItem
        active={thumbsState === "UP"}
        onClick={handleThumbs("UP")}
        space="2px"
      >
        {thumbsLoading === "UP" ? <LoadIcon /> : <ThumbsUpIcon />}
        <span>{thumbsUp}</span>
      </OptionalItem>
      <OptionalItem
        active={thumbsState === "DOWN"}
        onClick={handleThumbs("DOWN")}
        space="2px"
      >
        {thumbsLoading === "DOWN" ? <LoadIcon /> : <ThumbsUpIcon down />}
        <span>{thumbsDown}</span>
      </OptionalItem>
    </>
  );
};

export default memo(Thumbs);
