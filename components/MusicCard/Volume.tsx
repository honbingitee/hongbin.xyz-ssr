/*
 * @Author: hongbin
 * @Date: 2021-12-07 22:10:55
 * @LastEditors: hongbin
 * @LastEditTime: 2021-12-13 11:26:16
 * @Description:音量控制
 */
import { FC, MouseEventHandler, ReactElement } from "react";
import styled from "styled-components";
import { MuteSvg, VolumeSvg } from "../BUI/Icon";

interface IProps {
  volume: number;
  toggleMute: (val?: number) => void;
}

const Volume: FC<IProps> = ({ volume, toggleMute }): ReactElement => {
  const handleClick: MouseEventHandler<HTMLDivElement> = e => {
    const { y } = e.currentTarget.getBoundingClientRect();
    const { offsetHeight } = e.currentTarget;
    const valH = e.clientY - y;
    toggleMute(1 - valH / offsetHeight);
  };

  return (
    <Wrap volume={volume}>
      <span onClick={() => toggleMute()}>
        {volume ? <VolumeSvg /> : <MuteSvg />}
      </span>
      <div onClick={handleClick} />
    </Wrap>
  );
};

export default Volume;

const Wrap = styled.div<{ volume: number }>`
  width: 1.5rem;
  height: 100%;
  display: flex;
  flex-direction: column-reverse;

  & > div {
    /* opacity: 0; */
    height: 60%;
    width: 3px;
    border-radius: 100%;
    background-color: #fffae52b;
    margin: 0 auto;
    transition: opacity 0.3s linear, transform 0.2s linear;
    cursor: pointer;
    position: relative;
    overflow: hidden;

    ::after {
      content: "";
      height: 100%;
      width: 3px;
      position: absolute;
      left: 0;
      bottom: 0;
      background: #fffae5;
      transition: transform 0.2s linear;
      transform: ${({ volume }) => `scaleY(${volume})`};
      transform-origin: bottom;
    }
  }

  :hover {
    & > div {
      opacity: 1;
    }
  }

  svg {
    width: 1.5rem !important;
    height: 1.5rem !important;
    transform: rotate(-90deg);
  }
`;
