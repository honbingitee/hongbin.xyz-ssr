/*
 * @Author: hongbin
 * @Date: 2021-12-07 21:22:59
 * @LastEditors: hongbin
 * @LastEditTime: 2021-12-12 12:10:27
 * @Description:进度条
 */
import { FC, MouseEventHandler, ReactElement } from "react";
import styled from "styled-components";
import { flexCenter } from "../BUI/styled";

interface IProps {
  onChangePlayTime: (percent: number) => void;
}

const ProcessBar: FC<IProps> = ({ onChangePlayTime }): ReactElement => {
  const onMouseDown: MouseEventHandler<HTMLDivElement> = e => {
    const point = e.currentTarget;
    point.setAttribute("onDrag", "true");
    const { pageX } = e;
    point.onmousemove = e => {
      point.style.transform = `translateX(${e.pageX - pageX}px)`;
    };

    point.onmouseup = e => {
      point.setAttribute("onDrag", "");
      //   @ts-ignore
      const percent = (e.pageX - pageX) / point.parentElement.offsetWidth;
      point.style.transform = "";
      onChangePlayTime(percent);
      point.onmousemove = null;
    };
    point.onmouseleave = () => {
      point.setAttribute("onDrag", "");
      point.onmousemove = null;
    };
  };

  return (
    <Container>
      <Wrap>
        <Value id="music_card_progress_val" />
        <Block id="music_card_progress_point" onMouseDown={onMouseDown} />
      </Wrap>
    </Container>
  );
};

export default ProcessBar;

const Block = styled.div`
  width: 0.6rem;
  height: 0.5rem;
  position: absolute;
  left: 0;
  background-color: #fffae5;
  border-radius: 20%;
  top: -100%;
  transition-property: opacity;
  transition-duration: 0.2s;
  cursor: pointer;
  opacity: 0;
  :active {
    background-color: #b6b4af;
  }
`;

const Value = styled.div`
  background-color: #fffae5;
  position: absolute;
  top: 0;
  left: 0;
  height: inherit;
  width: 0;
`;

const Wrap = styled.div`
  width: 100%;
  height: 0.15rem;
  background-color: #fffae558;
  position: relative;
`;
const Container = styled.div`
  width: 70%;
  height: 1rem;
  margin: 0 auto;
  ${flexCenter};
  :hover ${Block} {
    opacity: 1;
  }
`;
