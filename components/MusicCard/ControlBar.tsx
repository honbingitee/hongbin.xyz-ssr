/*
 * @Author: hongbin
 * @Date: 2021-12-07 21:18:27
 * @LastEditors: hongbin
 * @LastEditTime: 2021-12-12 12:49:25
 * @Description:控制栏
 */
import { FC, ReactElement } from "react";
import styled from "styled-components";
import { PauseSvg, PlaySvg, RightArrowSvg, RightIcon } from "../BUI/Icon";

interface IProps {
  isPlay: boolean;
  togglePlayState: () => void;
}

const ControlBar: FC<IProps> = ({ isPlay, togglePlayState }): ReactElement => {
  return (
    <Wrap>
      <IconWrap>
        <RightArrowSvg />
      </IconWrap>
      <IconWrap onClick={togglePlayState}>
        {isPlay ? <PauseSvg /> : <PlaySvg />}
      </IconWrap>
      <IconWrap>
        <RightArrowSvg />
      </IconWrap>
    </Wrap>
  );
};

export default ControlBar;

const IconWrap = styled.div`
  cursor: pointer;
  width: 2rem;
  height: 2rem;
  border-radius: 3px;

  :first-child {
    transform: rotate(180deg);
  }

  svg {
    width: inherit;
    height: inherit;
  }
`;

const Wrap = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-evenly;
  flex: 1;
`;
