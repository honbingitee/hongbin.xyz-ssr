import styled, { css, keyframes } from "styled-components";
import { FlexDiv } from "./BUI";
import { ActiveLateY, SpaceStyle } from "./BUI/styled";
import { ThemeBgcAndText, ThemeButtonBgc } from "./BUI/View/ThemeView";
/*
 * @Author: hongbin
 * @Date: 2021-08-25 20:14:54
 * @LastEditTime: 2021-11-23 18:59:46
 * @Description: 公用的 styled 样式
 * @FilePath: /hongbin_xyz_web/components/commentStyled.ts
 */

export const MarkDownText = css`
  & > .markdown-body {
    color: ${props => (props.theme.mode === "light" ? undefined : "#fff")};
  }
`;

export const BlogTypeTag = styled.span`
  ${ThemeButtonBgc};
  color: #fff;
  border-radius: 2px;
  padding: 0.2rem 0.4rem;
  font-size: 0.8rem;
  font-weight: bold;
`;

export const OptionalItem = styled.div<{ space?: string; active?: boolean }>`
  min-height: 25px;
  display: flex;
  align-items: center;
  justify-content: space-evenly;
  flex-wrap: wrap;
  border-radius: 12px;
  padding: 0 0.6rem;
  margin: 5px;
  background: ${props => props.theme.second};
  transition: all 0.2s linear;
  cursor: pointer;
  user-select: none;
  ${ActiveLateY};

  &:hover {
    background: #51f !important;
    color: #fff !important;
  }

  ${props =>
    props.active
      ? ` background: #51f;
          color: #fff;`
      : undefined}

  ${SpaceStyle};
`;
export const fadeIn = keyframes`
0%{opacity:0};
100%{opacity:1};
`;
export const leftSlideIn = keyframes`
0%{
  left:-100%;
  /* width: 0;
  height: 0; */
};
/* 50%{
  left:0;
} */
100%{
  left:0;
  width: 80vw;
  height: 100vh;
};
`;
export const fadeOut = keyframes`
  0%{opacity:1};
  100%{opacity:0;};
`;

export const ContentWidth = css`
  width: ${props => props.theme.contentWidth};
  max-width: 100vw;
`;

export const minHeight = css`
  min-height: calc(100vh - var(--nav-height));
`;

export const ContainerCss = css`
  ${ContentWidth};
  min-height: calc(100vh - var(--nav-height));
  margin: 0 auto;
`;

export const ThemeContainerCss = css`
  ${ContainerCss};
  ${ThemeBgcAndText};
`;

export const Container = styled(FlexDiv)`
  ${ContainerCss};
`;

export const ThemeContainer = styled(Container)`
  ${ThemeBgcAndText};
`;

export const ContentCss = css`
  /* width: ${props => props.theme.contentWidth}; */
  border-radius: 10px;
  padding: 1rem;
  margin: 1rem;
  /* box-shadow: 0 0 10px 0 #ccc; */
`;
/**
 媒体查询 暂设为两个级别 
 手机端  和 PC端移动
 */

export const hideInPhone = css`
  @media screen and (max-width: ${props => props.theme.media.phone}) {
    display: none;
  }
`;

export const showInPhone = css`
  @media screen and (max-width: ${props => props.theme.media.phone}) {
    display: flex;
  }
`;

export const hideInPC = css`
  @media screen and (min-width: ${props => props.theme.media.pc}) {
    display: none;
  }
`;
