import { FC, ReactElement } from "react";
import styled from "styled-components";
import { IWorldNews } from "../../api/worldNewsApi";
import { mongoIdToDate } from "../../utils";

interface IProps {
  news: IWorldNews;
}

const WorldNewsItem: FC<IProps> = ({
  news: {
    _id,
    sender: [sender],
    content,
  },
}): ReactElement => {
  return (
    <Item key={_id}>
      <span>{sender.username}</span>
      <section>
        <span>{mongoIdToDate(_id).toLocaleString()}</span>
        <br />
        {content}
      </section>
    </Item>
  );
};

export default WorldNewsItem;

const Item = styled.div`
  margin: 1rem 0;
  max-width: 80%;
  color: #fff;
  & > span {
    background: ${props => props.theme.shadow};
    border-top-left-radius: 0.2rem;
    border-top-right-radius: 0.2rem;
    padding: 0.1rem 0.3rem;
    border-bottom: 1px solid ${props => props.theme.lightText};
    font-size: 0.8rem;
    letter-spacing: 1px;
  }
  section {
    margin-top: 3px;
    span {
      letter-spacing: 1px;
      color: ${props => props.theme.lightText};
      font-size: 0.6rem;
    }
    background: var(--primary-color);
    border-radius: 0.3rem;
    padding: 0.2rem;
    border-top-left-radius: 0;
  }
`;
