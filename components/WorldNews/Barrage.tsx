import { FC, ReactElement, useEffect, useState } from "react";
import styled, { keyframes } from "styled-components";
import { IWorldNews } from "../../api/worldNewsApi";

interface IProps {
  barrage: IWorldNews;
}

const Barrage: FC<IProps> = ({ barrage }): ReactElement => {
  const [barrages, setBarrages] = useState<IWorldNews[]>([]);

  useEffect(() => {
    if (barrage) {
      setBarrages(prev => [...prev, barrage]);
      setTimeout(() => {
        setBarrages(prev => prev.filter(item => item._id != barrage._id));
      }, 3000);
    }
  }, [barrage]);

  return (
    <Wrap>
      {barrages.map(barrage => (
        <span key={barrage._id}>{barrage.content}</span>
      ))}
    </Wrap>
  );
};

export default Barrage;

const animate = keyframes`
0%{transform: translateX(100vw)};
100%{transform: translateX(-10vw)};
`;

const Wrap = styled.div`
  position: fixed;
  z-index: ${props => props.theme.index.fixed};
  top: 10rem;

  & > span {
    color: #fff;
    display: inline-block;
    font-weight: bold;
    font-size: 1.2rem;
    letter-spacing: 1px;
    transition: left 3s linear;
    animation: ${animate} 3s linear;
  }
`;
