import styled, { css } from "styled-components";
import { flexCenter } from "../BUI/styled";

const line = css`
  content: " ";
  display: block;
  width: 1rem;
  height: 2px;
  background: #ccc;
  position: absolute;
  transition: transform 0.6s ease, background 0.6s linear, height 0.6s linear;
  border-radius: 1px;
  transform-origin: center;
`;

const AnimateCancelIcon = styled.div`
  width: 2rem;
  height: 2rem;
  border-radius: 2rem;
  background: inherit;
  box-shadow: inherit;
  position: absolute;
  top: -0.5rem;
  right: -0.5rem;
  cursor: pointer;
  transition: transform 0.2s ease;
  ${flexCenter};
  &::after {
    ${line};
    transform: rotate(-45deg);
  }
  &::before {
    ${line};
    transform: rotate(45deg);
  }
  &:hover {
    transform: scale(1.1);
    &::after {
      transform: scale(0.8) rotate(-90deg) translateY(0.4rem);
      background: #000;
      height: 3px;
    }
    &::before {
      transform: scale(0.8) rotate(90deg) translateY(0.4rem);
      background: #000;
      height: 3px;
    }
  }
  &:active {
    transform: scale(1);
  }
  @media (max-width: ${props => props.theme.media.phone}) {
    width: 5rem;
    height: 3rem;
    &::before {
      width: 2rem;
      height: 4px;
    }
    &::after {
      width: 2rem;
      height: 4px;
    }
  }
`;
export default AnimateCancelIcon;
