import { useRouter } from "next/router";
import { FC, ReactElement } from "react";
import styled from "styled-components";
import { toggleMenuRef } from "../../layout/Header";
import { HomeSvg, MenuIcon, MessageSvg } from "../BUI/Icon";
import { openMessageRef } from "../WorldNews";

interface IProps {}

const MobileBar: FC<IProps> = (): ReactElement => {
  const { push } = useRouter();
  return (
    <Container>
      <MessageSvg
        onClick={() => {
          openMessageRef.current?.open();
        }}
      />
      <IconBox
        onClick={() => {
          // 使用location.halfname会刷新浏览器相当于重新加载
          push("/");
        }}
      >
        <HomeSvg />
      </IconBox>
      <MenuIcon
        onClick={() => {
          toggleMenuRef.current?.toggle();
        }}
      />
    </Container>
  );
};

export default MobileBar;

const IconBox = styled.div`
  width: 70px;
  height: 70px;
  background-color: inherit;
  border-radius: 50px;
  transform: translateY(-15px);
  display: flex;
  align-items: center;
  justify-content: center;
  box-shadow: inherit;
  svg {
    transform: scale(2);
    path {
      fill: ${props => props.theme.primary};
    }
  }
`;

const Container = styled.div`
  position: fixed;
  bottom: 10px;
  left: 3%;
  width: 94%;
  background-color: ${props =>
    props.theme.mode === "dark" ? "#333" : "#fffae5"};
  height: 70px;
  transition: background 0.1s linear;
  z-index: ${props => props.theme.index.mobile};
  display: flex;
  align-items: center;
  justify-content: space-evenly;
  border-radius: 10% 90% 27% 84% / 68% 16% 84% 32%;
  box-shadow: 2.2px 1.6px 3.6px -5px rgba(0, 0, 0, 0.03),
    8.8px 6.4px 10px -5px rgba(0, 0, 0, 0.038),
    25.3px 18.4px 24.1px -5px rgba(0, 0, 0, 0.043),
    77px 56px 80px -5px rgba(0, 0, 0, 0.06);

  [data-name] {
    path {
      fill: #666;
    }
    width: 40px;
    height: 40px;
    padding: 10px;
    :active {
      background-color: #949393ab;
      border-radius: 10px;
    }
  }

  @media (min-width: ${props => props.theme.media.phone}) {
    display: none;
  }
`;
